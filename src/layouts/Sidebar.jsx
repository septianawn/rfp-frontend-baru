import React, { useState } from 'react'
import { List } from 'react-bootstrap-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link } from 'react-router-dom'
import '../sidebar.css'
import { Button, Col, DropdownButton, Row } from 'react-bootstrap'
import DropdownItem from 'react-bootstrap/esm/DropdownItem'
// import DropdownButton from 'react-bootstrap/DropdownButton';


const Sidebar = ({ setKlik }) => {
  const [open, setOpen] = useState(true);
  const [statOpen, setStatOpen] = useState('open')
  const actionOpen = () => {
    if (open === true) {
      setStatOpen('')
      setOpen(false)
      setKlik('active')
    } else {
      setStatOpen('open')
      setOpen(true)
      setKlik('')
    }
  }


  return (
    <div className={"sidebar " + statOpen}>
      <Row>
        <Col>
          <div className="logo-details">
            <FontAwesomeIcon icon={['fas', 'code']} />
            <h2 onClick={() => actionOpen()} className="logo_name" >Rastek.ID</h2>
            <i class='bx bx-menu' id="btn" ></i>
            {/* <img style={{ width: "150px", height: "auto" }} src={Logoweb} class="rounded float-right" alt="Sample image" /> */}
          </div>
        </Col>
        <Col>
          <Button variant='outline-light' onClick={() => actionOpen()}><List /></Button>
        </Col>
      </Row>

      <ul className="nav-list">
      <li>
          <FontAwesomeIcon icon={['fas fa-tractor']} />
             
             <span className="tooltip">Search</span>
          </li>
          <li>
            <Link to="#">
            <FontAwesomeIcon icon="fa-solid fa-check-square" />
              <span className="links_name">Dashboard</span>
            </Link>
             <span className="tooltip">Dashboard</span>
          </li>
         <li>
           <Link to="/rfpTransaksi">
             {/* <i className='bx bx-heart' ></i> */}
             <span className="links_name">Rfp Transaksi</span>
           </Link>
           <span className="tooltip">Rfp Transaksi</span>
         </li>
         <DropdownButton
          variant="outline-primary"
          id="dropdown-basic-button"
          title=" Menu RFP "
          className="ml-3">
            <DropdownItem href='/rfpPetty'> RFP Petty Cash </DropdownItem>
            <DropdownItem href='/rfpUmum'> RFP Umum </DropdownItem>
         </DropdownButton>
         <DropdownButton
          variant="outline-primary"
          id="dropdown-basic-button"
          title=" Data Master "
          className="ml-3" 
          >
            <DropdownItem href='/rekening'> Rekening </DropdownItem>
            <DropdownItem href='/client'> Client </DropdownItem>
            <DropdownItem href='/bank'> Bank </DropdownItem>
            <DropdownItem href='/vendor'> Vendor </DropdownItem>
            <DropdownItem href='/coaCategory'> Coa Category </DropdownItem>
            <DropdownItem href='/division'> Rekening </DropdownItem>
         </DropdownButton>
         <DropdownButton
          variant="outline-primary"
          id="dropdown-basic-button"
          title=" Menu User "
          className="ml-3" 
          >
            <DropdownItem href='/role'> Role User </DropdownItem>
            <DropdownItem href='/rfpUmum'> RFP Umum </DropdownItem>
         </DropdownButton>
         <li>
           <Link to="/authRegister">
             {/* <i className='bx bx-heart' ></i> */}
             <span className="links_name">Register</span>
           </Link>
           <span className="tooltip">Register</span>
         </li>
        <li className="profile">
          <div className="profile-details">
            {/* <!--<img src="profile.jpg" alt="profileImg">--> */}
            <div className="name_job">
              <div className="name">Username</div>
              <div className="job">Role</div>
            </div>
          </div>
          <i className='bx bx-log-out' id="log_out" ></i>
        </li>
      </ul>
    </div>
  )
}

export default Sidebar