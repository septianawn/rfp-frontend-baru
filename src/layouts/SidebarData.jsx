import React from "react";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import * as IoIcons from "react-icons/io";
import * as RiIcons from "react-icons/ri";
import * as BsIcons from "react-icons/bs";

export const SidebarData = [
{
	title: "Dashboard",
	path: "/dashboard",
	icon: <BsIcons.BsFillHouseDoorFill />
},
{
	title: "Contact",
	path: "/contact",
	icon: <BsIcons.BsTelephone />,
},
{
	title: "Rfp",
	icon: <BsIcons.BsWallet />,
	iconClosed: <RiIcons.RiArrowDownSFill />,
	iconOpened: <RiIcons.RiArrowUpSFill />,

	subNav: [
	{
		title: "Petty Cash",
		path: "/rfpPetty",
		icon: <BsIcons.BsCurrencyDollar />,
		cName: "sub-nav",
	},
	{
		title: "Umum",
		path: "/rfpUmum",
		icon: <BsIcons.BsBag />,
		cName: "sub-nav",
	},
]
},
{
	title: "Master",
	icon: <BsIcons.BsPeople />,
	iconClosed: <RiIcons.RiArrowDownSFill />,
	iconOpened: <RiIcons.RiArrowUpSFill />,

	subNav: [
	{
		title: "Division",
		path: "/division",
		icon: <BsIcons.BsRecord />,
		cName: "sub-nav",
	},
	{
		title: "Vendor",
		path: "/vendor",
		icon: <BsIcons.BsRecord />,
		cName: "sub-nav",
	},
	{
		title: "Bank",
		path: "/bank",
		icon: <BsIcons.BsRecord />,
	},
	{
		title: "Rekening",
		path: "/rekening",
		icon: <BsIcons.BsRecord />,
	},
	{
		title: "Coa Category",
		path: "/coaCategory",
		icon: <BsIcons.BsRecord />,
	},
	{
		title: "Coa",
		path: "/coa",
		icon: <BsIcons.BsRecord />,
	},
	{
		title: "Project",
		path: "/project",
		icon: <BsIcons.BsRecord />,
	},
	{
		title: "Client",
		path: "/client",
		icon: <BsIcons.BsRecord />,
	},
	],
},
{
	title: "Auth",
	// path: "/services",
	icon: <BsIcons.BsShieldLock />,
	iconClosed: <RiIcons.RiArrowDownSFill />,
	iconOpened: <RiIcons.RiArrowUpSFill />,

	subNav: [
	{
		title: "User",
		path: "/user",
		icon: <BsIcons.BsPerson />,
		cName: "sub-nav",
	},
	{
		title: "Role",
		path: "/role",
		icon: <BsIcons.BsGear />,
		cName: "sub-nav",
	},
	// {
	// 	title: "Register",
	// 	path: "/authRegister",
	// 	icon: <IoIcons.IoIosPaper />,
	// },
	],
},
{
	title: "Support",
	path: "/support",
	icon: <BsIcons.BsQuestionCircle />,
},
];
