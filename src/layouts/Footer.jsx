import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function Footer() {
  return (
    <div className="bottom-fixed" style={{
      height: '62px',
      background: "#161a24",
      marginBottom: '0',
      position: 'fixed',
      bottom: 0,
      width: '100%'
    }}>
      <Container>
        <Row>

          <Col md={12} style={{ alignSelf: 'center', color: 'white' }}>
            Rastek.ID
          </Col>

        </Row>
      </Container>
    </div>
  );
}

export default Footer;