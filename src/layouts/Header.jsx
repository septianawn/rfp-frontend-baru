import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { Link } from 'react-router-dom';
import '../style.css'

const divStyles = {
    boxShadow: '1px 2px 9px grey',
    position: 'fixed',
    width: '100%'
};
function Header () {
  return (
    <Navbar style={divStyles} bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="#">Navbar scroll</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScroll
          >
            <Link to="/about">
                <Button variant="light">About</Button>
            </Link>
            <Link to="/logout">
                <Button variant="light">Logout</Button>
            </Link>
            <Nav.Link href="#action2">Link</Nav.Link>
            <NavDropdown title="Link" id="navbarScrollingDropdown">
              <NavDropdown.Item href="#action3">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action4">
                Another action
              </NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action5">
                Something else here
              </NavDropdown.Item>
            </NavDropdown>
            <Nav.Link href="#" disabled>
              Link
            </Nav.Link>
          </Nav>
          <Form className="d-flex">
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Header;
// import Button from 'react-bootstrap/Button';
// import Container from 'react-bootstrap/Container';
// import Form from 'react-bootstrap/Form';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';
// import { Link } from 'react-router-dom';

// function Header() {
//   return (

//     // <Navbar bg="dark" variant="dark">
//     //     <Container>
//     //       <Navbar.Brand href="#home">Navbar</Navbar.Brand>
//     //       <Nav className="me-auto">
//     //         <Link to="/"><Button>Home</Button></Link>
//     //         <Link to="/features"><Button>Features</Button></Link>
//     //         <Link to="/pricing"><Button>Pricing</Button></Link>
//     //       </Nav>
//     //     </Container>
//     //   </Navbar>
//     // <Navbar bg="light" expand="lg">
//     //   <Container fluid>
//     //     <Navbar.Brand href="#">Navbar scroll</Navbar.Brand>
//     //     <Navbar.Toggle aria-controls="navbarScroll" />
//     //     <Navbar.Collapse id="navbarScroll">
//     //       <Nav
//     //         className="me-auto my-2 my-lg-0"
//     //         style={{ maxHeight: '100px' }}
//     //         navbarScroll
//     //       >
//     //         <Nav.Link href="#action1">Home</Nav.Link>
//     //         <Nav.Link href="#action2">Link</Nav.Link>
//     //         <NavDropdown title="Link" id="navbarScrollingDropdown">
//     //           <NavDropdown.Item href="#action3">Action</NavDropdown.Item>
//     //           <NavDropdown.Item href="#action4">
//     //             Another action
//     //           </NavDropdown.Item>
//     //           <NavDropdown.Divider />
//     //           <NavDropdown.Item href="#action5">
//     //             Something else here
//     //           </NavDropdown.Item>
//     //         </NavDropdown>
//     //         <Nav.Link href="#" disabled>
//     //           Link
//     //         </Nav.Link>
//     //       </Nav>
//     //       <Form className="d-flex">
//     //         <Form.Control
//     //           type="search"
//     //           placeholder="Search"
//     //           className="me-2"
//     //           aria-label="Search"
//     //         />
//     //         <Button variant="outline-success">Search</Button>
//     //       </Form>
//     //     </Navbar.Collapse>
//     //   </Container>
//     // </Navbar>
//   );
// }

// export default Header;