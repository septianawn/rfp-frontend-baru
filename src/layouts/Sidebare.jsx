import React, { useState } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import * as BsIcons from "react-icons/bs"
import { BiLogOut } from "react-icons/bi";
import { SidebarData } from "./SidebarData";
import SubMenu from "./SubMenu";
import { IconContext } from "react-icons/lib";
import { Col, Row } from "react-bootstrap";

const Nav = styled.div`
background: #212736;
height: 80px;
display: flex;
justify-content: flex-start;
align-items: center;
z-index: 9;
`;

const NavIcon = styled(Link)`
margin-left: 2rem;
font-size: 2rem;
height: 80px;
display: flex;
justify-content: flex-start;
align-items: center;
`;

const SidebarNav = styled.nav`
background: #161a24;
width: 300px;
height: 100vh;
display: flex;
justify-content: center;
position: fixed;
top: 0;
margin-left: 0px;
left: ${({ sidebar }) => (sidebar ? "0" : "-100%")};
transition: 350ms;
z-index: 10;
overflow: hidden;
`;

const SidebarWrap = styled.div`
width: 100%;
overflow: auto; 
&::-webkit-scrollbar{
	width : 1px;
};
&::-webkit-scrollbar-track{
	box-shadow: inset 0 0 0px transparent;
};
&::-webkit-scrollbar-thumb{
	background: transparent;
}
`;
const ProfileSide = styled.div`
width: 100%;
display: flex;
flex-direction: row;
padding: 10px 30px;
`;
const ProfileSideTitle = styled.div`
display: flex;
flex-direction: column;
margin-left: 15px;
`;


const Sidebare = ({ setKlik }) => {
	const [sidebar, setSidebar] = useState(true);

	const showSidebar = () => {
		setSidebar(!sidebar)
		setKlik(!sidebar)
	};
	const logout = () => {
		localStorage.clear()
		return window.location.reload();
	};

	return (
		<>
			<IconContext.Provider value={{ color: "#fff" }}>
				<Nav style={{ position: 'fixed', width: '100%', height: '65px', boxShadow: '1px 2px 9px grey', }}>
				<NavIcon style={{ marginLeft: (!sidebar ? '20px' : '310px') }} to="#">
						{
							!sidebar ? <FaIcons.FaBars style={{ width: '25px', }} onClick={showSidebar} /> :
								<BsIcons.BsXLg style={{ width: '25px' }} onClick={showSidebar} />

						}

					</NavIcon>
					{/* disini pake onclick buat logout trs hapus token */}
					<BiLogOut onClick={() => logout()} style={{ marginLeft: 'auto', marginRight: '20px', fontSize: '25px' }} />
				</Nav>
				<SidebarNav sidebar={sidebar}>
					<SidebarWrap>
						<NavIcon to="#">
							<img style={{ width: '180px' }} src={process.env.PUBLIC_URL + '/assets/img/logoR.png'} alt="images" />
						</NavIcon>
						<hr style={{ margin: '8px 20px 20px', height: '2px', backgroundColor: '#1E8E8E' }} />
						<ProfileSide>
							
							<img style={{ width: '60px' }} src={process.env.PUBLIC_URL + '/assets/img/user.png'} alt="images" />
							<ProfileSideTitle style={{alignSelf: 'center'}}>
								<h5 style={{ color: 'white', marginBottom: 2 }}>Rafy</h5>
									<p style={{ color: 'grey', fontSize: 13, marginBottom: 0 }}>Developer</p>
							</ProfileSideTitle>
						</ProfileSide>


						<hr style={{ margin: '8px 20px 20px', height: '2px', backgroundColor: '#1E8E8E' }} />

						{SidebarData.map((item, index) => {
							return <SubMenu item={item} key={index} />;
						})}
					</SidebarWrap>
				</SidebarNav>
			</IconContext.Provider>
		</>
	);
};

export default Sidebare;
