import React, { useState } from 'react'
import Footer from './Footer.jsx';
import Header from './Header.jsx';
import Sidebar from './Sidebar.jsx';
import Sidebare from './Sidebare.jsx';
import '../sidebar.css';

const Index = ({ children }) => {
  const [klik, setKlik] = useState(true);
  

  return (
    <>

      {/* <Header /> */}
      {/* <Sidebar setKlik={setKlik} />
      <div className={"klik " + klik}>
        
      </div> */}
      <Sidebare setKlik={setKlik}/>
      <div className={"klik " + (klik ? '' : 'active')}>
        {children}
      </div>
      <Footer />
    </>
  )
}

export default Index