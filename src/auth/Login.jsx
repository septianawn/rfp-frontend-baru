import React, { Component, useEffect, useState } from 'react';
import { Card } from 'react-bootstrap';
// import { Link } from 'react-router-dom';
import Logoweb from '../image/Logoweb.png'
import '../login.css'

const Login = ({ token }) => {

    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [validation, setValidation] = useState([
        {
            usn: false, pw: false
        }
    ]);

    // untuk get api
    const _submit = async (e) => {
        // agar tidak perlu load form login saat submit
        e.preventDefault();
        let usn = userName;
        let pw = password;
        let validate = [...validation]
        let validateStatus = false;
        if (usn === '') {
            validate[0].usn = true
            validateStatus = true
        }
        else {
            validate[0].usn = false
            validateStatus = false
        }
        if (pw === '') {
            validate[0].pw = true
            validateStatus = true
        }
        else {
            validate[0].pw = false
            validateStatus = false
        }
        setValidation(validate)
    
        // menghubungkan ke be dgn api
        const response = await fetch("http://10.200.0.44:8080/api/auth/login", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userName: userName,
                password: password
            }),
        });
        // kondisi jika respon di be 200, maka akan ditampilkan pesan di console
        if (response.status === 200) {
            const json = await response.json();
            console.log("Anda berhasil login")
            token(json.token)
            localStorage.setItem("token", json.token);

        }else if (validateStatus === false){
            // validate untuk passw
            alert("username atau password salah")
            console.log("Gagal login")
            token(null)
            
        }else {
            console.log("Gagal Login")
            token(null)
        }
    }

    const [userPassword, setUserPassword] = useState(false);
    return (
        <div className="container" style={{ paddingTop: '50px' }}>
            <div className="login row justify-content-center" style={{ paddingLeft: '250px', paddingRight: '250px' }}>

                <Card>
                    <Card.Body>
                        <div className='logo col-md-12' style={{ textAlign: 'center' }} >
                            <img style={{ width: "150px", height: "auto" }} src={Logoweb} class="rounded float-right" alt="Sample image" />
                        </div>
                        <div className="judul col-md-12">
                            <h2>Login Form</h2>
                            <form>
                                <div className='text-box'>
                                    <div className="form-group">
                                        <label for="exampleInputUsernamel1">Username </label>
                                        <input style={{ border: (validation[0].usn ? '1px solid red' : '1px solid #ced4da') }}
                                            onChange={(e) => setUserName(e.target.value)} type="username" className="form-control" placeholder="Masukan username anda" required />
                                        {validation[0].usn ? (<span style={{ color: 'red' }}><h6>username tidak boleh kosong</h6></span>) : null}
                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputPassword1">Password</label>
                                       <input style={{ border: (validation[0].pw ? '1px solid red' : '1px solid #ced4da') }}
                                            onChange={(e) => setPassword(e.target.value)} type={userPassword ? 'text' : 'password'} className="form-control" placeholder="Password" />
                                        {validation[0].pw ? (<span style={{ color: 'red' }}><h6>password tidak boleh kosong</h6> </span>) : null}
                                        <h6><input type="checkbox" onChange={(e) => setUserPassword(!userPassword)} /> Show Password</h6>
                                    </div>
                                    <div className='form-login'>
                                        <button type="submit" class="btn btn-primary btn-lg btn-block" onClick={(e) => { _submit(e) }}>Login</button>
                                        {/* <p className="small fw-bold mt-2 pt-1 mb-1">Don't have an account? <a href="#!" className="link-danger">Register</a></p> */}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </Card.Body>
                </Card>

            </div>
        </div>

    );
}

export default Login;