import React, { useState } from 'react';
import { Card, Container } from 'react-bootstrap';

const RoleId = ({ token }) => {

    //define state
    const [name, setName] = useState("");
   

    //define state validation
    const [validation, setValidation] = useState([]);


    //function "registerHanlder"
    const registerHandler = async (e) => {
        e.preventDefault();

        //initialize formData
        const formData = new FormData();

        //append data to formData
        formData.append('name', name);

        const _submit = async(e) => {
            // agar tidak perlu load form login saat submit
            e.preventDefault();
            // menghubungkan ke be dgn api
            const response = await fetch("http://10.200.0.44:8080/api/RoleRfp", {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: name,
                }),
            });
            // kondisi jika respon di be 200, maka akan ditampilkan pesan di console
            if(response.status === 200){
                const json = await response.json();
                console.log("Anda berhasil menginputkan Role")
                token(json.token)
                localStorage.setItem("token",json.token);
            }
            else{
                console.log("Gagal")
                token(null)
            }
        }
    };

    return (
        <Container style={{ paddingTop: '80px' }}>
            <Card >
                <Card.Body>
                    <form>
                        <div className="judul col-md-12">
                            <h2 className="fw-bold text-center mb-5">Role</h2>
                            <hr />
                            <form>
                                <div className='text-box'>
                                    <div className="form-group">
                                        <form onSubmit={registerHandler}>
                                            <div className="col">
                                                <div className="col-md-6">
                                                    <div className="mb-3">
                                                        <label className="form-label">Name</label>
                                                        <input type="text" className="form-control" value={name} onChange={(e) => setName(e.target.value)} placeholder="Masukkan Role" />
                                                    </div>
                                                    {
                                                        validation.name && (
                                                            <div className="alert alert-danger">
                                                                {validation.name[0]}
                                                            </div>
                                                        )
                                                    }
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="mb-7"></div>
                                                <button style={{ width : "100px"}} type="submit" className="btn btn-primary">submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </form>
                </Card.Body>
            </Card>
        </Container>
    )

}

export default RoleId;