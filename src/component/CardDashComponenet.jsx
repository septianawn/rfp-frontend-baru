import React from 'react'
import { Card, Col, Row } from 'react-bootstrap';
import * as FaIcons from "react-icons/fa";
import "../style.css";


const CardDashComponenet = ({bgcolor, data}) => {
    // console.log(value)
  return (
    <Card style={{background: bgcolor, color: 'white', borderColor: bgcolor, borderRadius: '15px'}}>
        <Card.Body style={{zIndex: 1}}>
        <Row>
            <Col xs={12} sm={12} md={12} lg={12} style={{marginBottom: 10}}>
                <span style={{fontSize: '15px', fontWeight: 'bold'}}>{data[0].title}</span>
            </Col>
            <Col xs={6} sm={6} md={6} lg={6} style={{alignSelf: 'center'}}>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                    <span style={{fontSize: '15px'}}>RFP Petty Cash</span>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12}>
                    <h4 style={{fontWeight: 'bold',marginBottom: 0}}>{data[0].value}</h4>                    
                    </Col>
                </Row>
            </Col>
            <Col xs={6} sm={6} md={6} lg={6} style={{alignSelf: 'center', padding: 0}}>
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={12}>
                        <span style={{fontSize: '15px'}}>RFP Umum</span>
                        </Col>
                        <Col xs={12} sm={12} md={12} lg={12}>
                        <h4 style={{fontWeight: 'bold',marginBottom: 0}}>{data[0].valueUmum}</h4>                    
                        </Col>
                    </Row>
            </Col>
        </Row>
        </Card.Body>
        
        <div className='background-icon'>
            <FaIcons.FaHandHoldingUsd/>
        </div>
    </Card>
  )
}

export default CardDashComponenet