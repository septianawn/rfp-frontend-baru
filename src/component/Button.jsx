import React from 'react'
import styled from 'styled-components';

const ButtonPrimary = styled.button
  `
  background: transparent;
  border: 1.9px solid #212736;
  color: #212736;
  padding: 5px 10px;
  border-radius: 7px;
  width: auto;
  margin-right: 5px;
  &:hover{
    background: #212736;
    color: white;
	cursor: pointer;
  }
  `;

export {ButtonPrimary}