import React from 'react'
import { Card } from 'react-bootstrap'

const CardComponent = ({children, style}) => {
  return (
    <Card style={style}>
        <Card.Body>
            {children}
        </Card.Body>
    </Card>
  )
}

export default CardComponent