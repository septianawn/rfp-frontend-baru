import React from 'react'
import * as BsIcons from "react-icons/bs";

const BreadcrumbComponent = ({page=[]}) => {
    return (
        <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
                {/* <BsIcons.BsFillHouseDoorFill /> */}
                <li className="breadcrumb-item active" aria-current="page">Home</li>
                {page.map((item)=>(
                    <li className="breadcrumb-item active" aria-current="page">{item.name}</li>
                ))}
            </ol>
        </nav>
    )
}

export default BreadcrumbComponent