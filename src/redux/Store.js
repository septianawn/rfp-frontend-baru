import { configureStore } from "@reduxjs/toolkit"
import bankReducer from './slice/BankSlice'
import coaCategoryReducer from './slice/CoaCategorySlice'
import clientReducer from './slice/ClientSlice'
import divisionReducer from './slice/DivisionSlice'
import vendorReducer from './slice/VendorSlice'
import coaReducer from './slice/CoaSlice'
import rekeningReducer from './slice/RekeningSlice'

export const store = configureStore({
    reducer: {
        bank : bankReducer,
        coaCategory: coaCategoryReducer,
        client: clientReducer,
        division: divisionReducer,
        vendor: vendorReducer,
        coa: coaReducer,
        rekening: rekeningReducer
    }
})