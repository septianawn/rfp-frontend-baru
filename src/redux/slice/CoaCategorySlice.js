import {createSlice} from "@reduxjs/toolkit"
import { getCoaCategoryList, postCoaCategory ,editCoaCategory, deleteCoaCategory} from "../action/CoaCategoryAction"

const initialState = {
    dataCoaCategory : [],
    isLoading: false,
    isOpen: false,
    state: 'add',
    dataByIdCoaCategory: [],
    idData: 0
}

const coaCategorySlice = createSlice({
    name : 'coaCategory',
    initialState,
    reducers: {
        toggleModal : (state, action) => {
            state.isOpen = !state.isOpen
            state.state = action.payload.state
            if(state.state==='edit' || state.state==='delete'){
                state.dataByIdCoaCategory=action.payload.data
                state.idData=action.payload.id
            }
        }
    },
    extraReducers: {
        [getCoaCategoryList.pending]:(state)=>{    
            state.isLoading= true
        },
        [getCoaCategoryList.fulfilled]:(state,action)=>{
            // console.log(action.payload)
            state.dataCoaCategory= action.payload
            state.isLoading = false
        },
        [getCoaCategoryList.rejected]:(state)=>{

            state.isLoading= false
        },
        [postCoaCategory.pending]:(state)=>{
            state.isLoading= true
        },
        [postCoaCategory.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
        },
        [postCoaCategory.rejected]:(state)=>{

            state.isLoading= false
        },
        [editCoaCategory.pending]:(state)=>{
            state.isLoading= true
        },
        [editCoaCategory.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
            state.dataByIdCoaCategory=[]
            state.idData=0
        },
        [editCoaCategory.rejected]:(state)=>{

            state.isLoading= false
        },
        [deleteCoaCategory.pending]:(state)=>{    
            state.isLoading= true
        },
        [deleteCoaCategory.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
            state.dataByIdCoaCategory=[]
            state.idData=0
        },
        [deleteCoaCategory.rejected]:(state)=>{

            state.isLoading= false
        }
    }
})

export const {toggleModal} = coaCategorySlice.actions;
export default coaCategorySlice.reducer;