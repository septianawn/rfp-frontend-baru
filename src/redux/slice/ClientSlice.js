import {createSlice} from "@reduxjs/toolkit"
import { getClientList, postClient ,editClient, deleteClient} from "../action/ClientAction"

const initialState = {
    dataClient : [],
    isLoading: false,
    isOpen: false,
    state: 'add',
    dataById: [],
    idData: 0
}

const clientSlice = createSlice({
    name : 'client',
    initialState,
    reducers: {
        toggleModal : (state, action) => {
            state.isOpen = !state.isOpen
            state.state = action.payload.state
            if(state.state==='edit' || state.state==='delete'){
                state.dataById=action.payload.data
                state.idData=action.payload.id
            }
        }
    },
    extraReducers: {
        [getClientList.pending]:(state)=>{    
            state.isLoading= true
        },
        [getClientList.fulfilled]:(state,action)=>{
            // console.log(action.payload)
            state.dataClient= action.payload
            state.isLoading = false
        },
        [getClientList.rejected]:(state)=>{

            state.isLoading= false
        },
        [postClient.pending]:(state)=>{
            state.isLoading= true
        },
        [postClient.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
        },
        [postClient.rejected]:(state)=>{

            state.isLoading= false
        },
        [editClient.pending]:(state)=>{
            state.isLoading= true
        },
        [editClient.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
            state.dataById=[]
            state.idData=0
        },
        [editClient.rejected]:(state)=>{

            state.isLoading= false
        },
        [deleteClient.pending]:(state)=>{    
            state.isLoading= true
        },
        [deleteClient.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
            state.dataById=[]
            state.idData=0
        },
        [deleteClient.rejected]:(state)=>{

            state.isLoading= false
        }
    }
})

export const {toggleModal} = clientSlice.actions;
export default clientSlice.reducer;