import {createSlice} from "@reduxjs/toolkit"
import { getCoaList, postCoa ,editCoa, deleteCoa} from "../action/CoaAction"

const initialState = {
    dataCoa : [],
    isLoading: false,
    isOpen: false,
    state: 'add',
    dataById: [],
    idData: 0
}

const coaSlice = createSlice({
    name : 'coa',
    initialState,
    reducers: {
        toggleModal : (state, action) => {
            state.isOpen = !state.isOpen
            state.state = action.payload.state
            if(state.state==='edit' || state.state==='delete'){
                state.dataById=action.payload.data
                state.idData=action.payload.id
            }
        }
    },
    extraReducers: {
        [getCoaList.pending]:(state)=>{    
            state.isLoading= true
        },
        [getCoaList.fulfilled]:(state,action)=>{
            // console.log(action.payload)
            state.dataCoa= action.payload
            state.isLoading = false
        },
        [getCoaList.rejected]:(state)=>{

            state.isLoading= false
        },
        [postCoa.pending]:(state)=>{
            state.isLoading= true
        },
        [postCoa.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
        },
        [postCoa.rejected]:(state)=>{

            state.isLoading= false
        },
        [editCoa.pending]:(state)=>{
            state.isLoading= true
        },
        [editCoa.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
            state.dataById=[]
            state.idData=0
        },
        [editCoa.rejected]:(state)=>{

            state.isLoading= false
        },
        [deleteCoa.pending]:(state)=>{    
            state.isLoading= true
        },
        [deleteCoa.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
            state.dataById=[]
            state.idData=0
        },
        [deleteCoa.rejected]:(state)=>{

            state.isLoading= false
        }
    }
})

export const {toggleModal} = coaSlice.actions;
export default coaSlice.reducer;