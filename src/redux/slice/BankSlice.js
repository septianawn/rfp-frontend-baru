import {createSlice} from "@reduxjs/toolkit"
import { getBankList, postBank ,editBank, deleteBank} from "../action/BankAction"

const initialState = {
    dataBank : [],
    isLoading: false,
    isOpen: false,
    state: 'add',
    dataById: [],
    idData: 0
}

const bankSlice = createSlice({
    name : 'bank',
    initialState,
    reducers: {
        toggleModal : (state, action) => {
            state.isOpen = !state.isOpen
            console.log('sikat')
            state.state = action.payload.state
            if(state.state==='edit' || state.state==='delete'){
                state.dataById=action.payload.data
                state.idData=action.payload.id
            }
        }
    },
    extraReducers: {
        [getBankList.pending]:(state)=>{    
            state.isLoading= true
        },
        [getBankList.fulfilled]:(state,action)=>{
            // console.log(action.payload)
            state.dataBank= action.payload
            state.isLoading = false
        },
        [getBankList.rejected]:(state)=>{

            state.isLoading= false
        },
        [postBank.pending]:(state)=>{
            state.isLoading= true
        },
        [postBank.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
        },
        [postBank.rejected]:(state)=>{

            state.isLoading= false
        },
        [editBank.pending]:(state)=>{
            state.isLoading= true
        },
        [editBank.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
            state.dataById=[]
            state.idData=0
        },
        [editBank.rejected]:(state)=>{

            state.isLoading= false
        },
        [deleteBank.pending]:(state)=>{    
            state.isLoading= true
        },
        [deleteBank.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
            state.dataById=[]
            state.idData=0
        },
        [deleteBank.rejected]:(state)=>{

            state.isLoading= false
        }
    }
})

export const {toggleModal} = bankSlice.actions;
export default bankSlice.reducer;