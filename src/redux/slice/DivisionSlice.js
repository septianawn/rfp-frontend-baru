import {createSlice} from "@reduxjs/toolkit"
import { getDivisionList, postDivision ,editDivision, deleteDivision} from "../action/DivisionAction"

const initialState = {
    dataDivision : [],
    isLoading: false,
    isOpen: false,
    state: 'add',
    dataById: [],
    idData: 0
}

const divisionSlice = createSlice({
    name : 'division',
    initialState,
    reducers: {
        toggleModal : (state, action) => {
            state.isOpen = !state.isOpen
            state.state = action.payload.state
            if(state.state==='edit' || state.state==='delete'){
                state.dataById=action.payload.data
                state.idData=action.payload.id
            }
        }
    },
    extraReducers: {
        [getDivisionList.pending]:(state)=>{    
            state.isLoading= true
        },
        [getDivisionList.fulfilled]:(state,action)=>{
            // console.log(action.payload)
            state.dataDivision= action.payload
            state.isLoading = false
        },
        [getDivisionList.rejected]:(state)=>{

            state.isLoading= false
        },
        [postDivision.pending]:(state)=>{
            state.isLoading= true
        },
        [postDivision.fulfilled]:(state,action)=>{
            console.log('hadir')
            state.isOpen=false
            state.isLoading = false
        },
        [postDivision.rejected]:(state)=>{

            state.isLoading= false
        },
        [editDivision.pending]:(state)=>{
            state.isLoading= true
        },
        [editDivision.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
            state.dataById=[]
            state.idData=0
        },
        [editDivision.rejected]:(state)=>{

            state.isLoading= false
        },
        [deleteDivision.pending]:(state)=>{    
            state.isLoading= true
        },
        [deleteDivision.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
            state.dataById=[]
            state.idData=0
        },
        [deleteDivision.rejected]:(state)=>{

            state.isLoading= false
        }
    }
})

export const {toggleModal} = divisionSlice.actions;
export default divisionSlice.reducer;