import {createSlice} from "@reduxjs/toolkit"
import { getRekeningList, postRekening ,editRekening, deleteRekening} from "../action/RekeningAction"

const initialState = {
    dataRekening : [],
    isLoading: false,
    isOpen: false,
    state: 'add',
    dataById: [],
    idData: 0
}

const rekeningSlice = createSlice({
    name : 'rekening',
    initialState,
    reducers: {
        toggleModal : (state, action) => {
            state.isOpen = !state.isOpen
            state.state = action.payload.state
            if(state.state==='edit' || state.state==='delete'){
                state.dataById=action.payload.data
                state.idData=action.payload.id
            }
        }
    },
    extraReducers: {
        [getRekeningList.pending]:(state)=>{    
            state.isLoading= true
        },
        [getRekeningList.fulfilled]:(state,action)=>{
            // console.log(action.payload)
            state.dataRekening= action.payload
            state.isLoading = false
        },
        [getRekeningList.rejected]:(state)=>{

            state.isLoading= false
        },
        [postRekening.pending]:(state)=>{
            state.isLoading= true
        },
        [postRekening.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
        },
        [postRekening.rejected]:(state)=>{

            state.isLoading= false
        },
        [editRekening.pending]:(state)=>{
            state.isLoading= true
        },
        [editRekening.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
            state.dataById=[]
            state.idData=0
        },
        [editRekening.rejected]:(state)=>{

            state.isLoading= false
        },
        [deleteRekening.pending]:(state)=>{    
            state.isLoading= true
        },
        [deleteRekening.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
            state.dataById=[]
            state.idData=0
        },
        [deleteRekening.rejected]:(state)=>{

            state.isLoading= false
        }
    }
})

export const {toggleModal} = rekeningSlice.actions;
export default rekeningSlice.reducer;