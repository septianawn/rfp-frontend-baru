import {createSlice} from "@reduxjs/toolkit"
import { getVendorList, postVendor ,editVendor, deleteVendor} from "../action/VendorAction"

const initialState = {
    dataVendor : [],
    isLoading: false,
    isOpen: false,
    state: 'add',
    dataById: [],
    idData: 0
}

const vendorSlice = createSlice({
    name : 'division',
    initialState,
    reducers: {
        toggleModal : (state, action) => {
            state.isOpen = !state.isOpen
            state.state = action.payload.state
            if(state.state==='edit' || state.state==='delete'){
                state.dataById=action.payload.data
                state.idData=action.payload.id
            }
        }
    },
    extraReducers: {
        [getVendorList.pending]:(state)=>{    
            state.isLoading= true
        },
        [getVendorList.fulfilled]:(state,action)=>{
            console.log(action.payload)
            state.dataVendor= action.payload
            state.isLoading = false
        },
        [getVendorList.rejected]:(state)=>{

            state.isLoading= false
        },
        [postVendor.pending]:(state)=>{
            state.isLoading= true
        },
        [postVendor.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
        },
        [postVendor.rejected]:(state)=>{

            state.isLoading= false
        },
        [editVendor.pending]:(state)=>{
            state.isLoading= true
        },
        [editVendor.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
            state.dataById=[]
            state.idData=0
        },
        [editVendor.rejected]:(state)=>{

            state.isLoading= false
        },
        [deleteVendor.pending]:(state)=>{    
            state.isLoading= true
        },
        [deleteVendor.fulfilled]:(state,action)=>{
            state.isOpen=false
            state.isLoading = false
            state.dataById=[]
            state.idData=0
        },
        [deleteVendor.rejected]:(state)=>{

            state.isLoading= false
        }
    }
})

export const {toggleModal} = vendorSlice.actions;
export default vendorSlice.reducer;