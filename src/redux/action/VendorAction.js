import { createAsyncThunk } from "@reduxjs/toolkit";

export const getVendorList = createAsyncThunk('vendor/list', async(thunkAPI)=> {
    const respone = await fetch("http://10.200.0.44:8080/api/vendor", {
            method: 'get',
            headers: new Headers({

                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true',
            })
        })
        const responeResult = respone.json();
        // console.log("as")
        return responeResult
});

export const postVendor = createAsyncThunk('vendor/post', async(param,thunkAPI)=> {
    console.log(param)
    const requestOptions = {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + localStorage.getItem('token'),
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': 'true'
        },
        body: JSON.stringify(param.data)
      };
      let response = await fetch('http://10.200.0.44:8080/api/vendor/', requestOptions)
      if(response.status === 200){
        thunkAPI.dispatch(getVendorList())
      }
        // .then(getApi())
});
export const editVendor = createAsyncThunk('vendor/update', async(param,thunkAPI)=> {
  console.log(param)
  const requestOptions = {
      method: 'PUT',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      },
      body: JSON.stringify(param.data)
    };
    let response = await fetch(`http://10.200.0.44:8080/api/vendor/${param.id}`, requestOptions)
    if(response.status === 200){
      thunkAPI.dispatch(getVendorList())
    }
      // .then(getApi())
});
export const deleteVendor = createAsyncThunk('vendor/delete', async(param,thunkAPI)=> {
  console.log(param)
  const requestOptions = {
      method: 'Delete',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      },
      body: JSON.stringify(param.data)
    };
    let response = await fetch(`http://10.200.0.44:8080/api/vendor/${param.id}`, requestOptions)
    if(response.status === 200){
      thunkAPI.dispatch(getVendorList())
    }
      // .then(getApi())
});