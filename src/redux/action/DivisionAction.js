import { createAsyncThunk } from "@reduxjs/toolkit";

export const getDivisionList = createAsyncThunk('division/list', async(thunkAPI)=> {
    const respone = await fetch("http://10.200.0.44:8080/api/division", {
            method: 'get',
            headers: new Headers({

                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true',
            })
        })
        const responeResult = respone.json();
        // console.log("as")
        return responeResult
});

export const postDivision = createAsyncThunk('division/post', async(param,thunkAPI)=> {
    console.log(param)
    const requestOptions = {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + localStorage.getItem('token'),
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': 'true'
        },
        body: JSON.stringify(param.data)
      };
      let response = await fetch('http://10.200.0.44:8080/api/division/', requestOptions)
      if(response.status === 200){
        thunkAPI.dispatch(getDivisionList())
      }
        // .then(getApi())
});
export const editDivision = createAsyncThunk('division/update', async(param,thunkAPI)=> {
  console.log(param)
  const requestOptions = {
      method: 'PUT',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      },
      body: JSON.stringify(param.data)
    };
    let response = await fetch(`http://10.200.0.44:8080/api/division/${param.id}`, requestOptions)
    if(response.status === 200){
      thunkAPI.dispatch(getDivisionList())
    }
      // .then(getApi())
});
export const deleteDivision = createAsyncThunk('division/delete', async(param,thunkAPI)=> {
  console.log(param)
  const requestOptions = {
      method: 'Delete',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      },
      body: JSON.stringify(param.data)
    };
    let response = await fetch(`http://10.200.0.44:8080/api/division/${param.id}`, requestOptions)
    if(response.status === 200){
      thunkAPI.dispatch(getDivisionList())
    }
      // .then(getApi())
});