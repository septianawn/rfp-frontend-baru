import { createAsyncThunk } from "@reduxjs/toolkit";
// import { API_URL } from "./content/bank/Bank";
// import { toggleModal } from "../slice/BankSlice";

export const getBankList = createAsyncThunk('bank/list', async(thunkAPI)=> {
    const respone = await fetch("http://10.200.0.44:8080/api/bank", {
            method: 'get',
            headers: new Headers({

                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true',
            })
        })
        const responeResult = respone.json();
        // console.log("as")
        return responeResult
});

export const postBank = createAsyncThunk('bank/post', async(param,thunkAPI)=> {
    console.log(param)
    const requestOptions = {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + localStorage.getItem('token'),
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': 'true'
        },
        body: JSON.stringify(param.data)
      };
      let response = await fetch('http://10.200.0.44:8080/api/bank/', requestOptions)
      if(response.status === 200){
        thunkAPI.dispatch(getBankList())
      }
        // .then(getApi())
});
export const editBank = createAsyncThunk('bank/update', async(param,thunkAPI)=> {
  console.log(param)
  const requestOptions = {
      method: 'PUT',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      },
      body: JSON.stringify(param.data)
    };
    let response = await fetch(`http://10.200.0.44:8080/api/bank/${param.id}`, requestOptions)
    if(response.status === 200){
      thunkAPI.dispatch(getBankList())
    }
      // .then(getApi())
});
export const deleteBank = createAsyncThunk('bank/delete', async(param,thunkAPI)=> {
  console.log(param)
  const requestOptions = {
      method: 'Delete',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      },
      body: JSON.stringify(param.data)
    };
    let response = await fetch(`http://10.200.0.44:8080/api/bank/${param.id}`, requestOptions)
    if(response.status === 200){
      thunkAPI.dispatch(getBankList())
    }
      // .then(getApi())
});