import { createAsyncThunk } from "@reduxjs/toolkit";

export const getRekeningList = createAsyncThunk('rekening/list', async(thunkAPI)=> {
    const respone = await fetch("http://10.200.0.44:8080/api/rekening", {
            method: 'get',
            headers: new Headers({

                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true',
            })
        })
        const responeResult = respone.json();
        // console.log("as")
        return responeResult
});

export const postRekening = createAsyncThunk('rekening/post', async(param,thunkAPI)=> {
    console.log(param)
    const requestOptions = {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + localStorage.getItem('token'),
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': 'true'
        },
        body: JSON.stringify(param.data)
      };
      let response = await fetch('http://10.200.0.44:8080/api/rekening/', requestOptions)
      if(response.status === 200){
        thunkAPI.dispatch(getRekeningList())
      }
        // .then(getApi())
});
export const editRekening = createAsyncThunk('rekening/update', async(param,thunkAPI)=> {
  console.log(param)
  const requestOptions = {
      method: 'PUT',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      },
      body: JSON.stringify(param.data)
    };
    let response = await fetch(`http://10.200.0.44:8080/api/rekening/${param.id}`, requestOptions)
    if(response.status === 200){
      thunkAPI.dispatch(getRekeningList())
    }
      // .then(getApi())
});
export const deleteRekening = createAsyncThunk('rekening/delete', async(param,thunkAPI)=> {
  console.log(param)
  const requestOptions = {
      method: 'Delete',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      },
      body: JSON.stringify(param.data)
    };
    let response = await fetch(`http://10.200.0.44:8080/api/rekening/${param.id}`, requestOptions)
    if(response.status === 200){
      thunkAPI.dispatch(getRekeningList())
    }
      // .then(getApi())
});