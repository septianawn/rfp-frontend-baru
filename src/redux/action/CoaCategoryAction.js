import { createAsyncThunk } from "@reduxjs/toolkit";
// import { API_URL } from "./content/bank/Bank";
// import { toggleModal } from "../slice/BankSlice";

export const getCoaCategoryList = createAsyncThunk('coaCategory/list', async(thunkAPI)=> {
    const respone = await fetch("http://10.200.0.44:8080/api/coa-category", {
            method: 'get',
            headers: new Headers({

                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true',
            })
        })
        const responeResult = respone.json();
        // console.log("as")
        return responeResult
});

export const postCoaCategory = createAsyncThunk('coaCategory/post', async(param,thunkAPI)=> {
    console.log(param)
    const requestOptions = {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + localStorage.getItem('token'),
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': 'true'
        },
        body: JSON.stringify(param.data)
      };
      let response = await fetch('http://10.200.0.44:8080/api/coa-category/', requestOptions)
      if(response.status === 200){
        thunkAPI.dispatch(getCoaCategoryList())
      }
        // .then(getApi())
});
export const editCoaCategory = createAsyncThunk('coaCategory/update', async(param,thunkAPI)=> {
  console.log(param)
  const requestOptions = {
      method: 'PUT',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      },
      body: JSON.stringify(param.data)
    };
    let response = await fetch(`http://10.200.0.44:8080/api/coa-category/${param.id}`, requestOptions)
    if(response.status === 200){
      thunkAPI.dispatch(getCoaCategoryList())
    }
      // .then(getApi())
});
export const deleteCoaCategory = createAsyncThunk('coaCategory/delete', async(param,thunkAPI)=> {
  console.log(param)
  const requestOptions = {
      method: 'Delete',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      },
      body: JSON.stringify(param.data)
    };
    let response = await fetch(`http://10.200.0.44:8080/api/coa-category/${param.id}`, requestOptions)
    if(response.status === 200){
      thunkAPI.dispatch(getCoaCategoryList())
    }
      // .then(getApi())
});