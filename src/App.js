import './App.css';
// import { Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link, Route, Routes } from 'react-router-dom';
import React, { useState } from 'react';
import Dashboard from './content/Dashboard';
import About from './content/About';
import Login from './auth/Login';
import 'bootstrap/dist/css/bootstrap.min.css';
import Rekening from './content/rekening/Rekening';
import Client from './content/client/Client';

import Index from './layouts/index';
import Sidebar from './layouts/Sidebar';
import Division from './content/division/Division';
import Register from './authRegister/Register';
import Bank from './content/bank/Bank';
import Vendor from './content/vendor/Vendor';
import Coa from './content/coa/Coa';
import UserData from './content/pengguna/UserData';
import Project from './content/project/Project';
import CoaCategory from './content/coaCategory/CoaCategory';
import RfpTransaksi from './content/rfpTransaksi/RfpTransaksi';
import RfpUmum from './content/rfpUmum/RfpUmum';
import RfpPettyCash from './content/rfpPetty/RfpPettyCash';
import RfpDetail from './content/rfpDetail/RfpDetail';
import Role from './content/RoleId/Role';

// alternatif lain untuk function
// const AppComponent = () => {
//   return(

//   )
// }

function App() {
  // agar token dapat ditampilkan di console
  const [token, setToken] = useState(localStorage.getItem("token"))
  if (token === null) {
    return <Login token={(data) => setToken(data)} />
  }
  return (
    <div className="App" style={{background:'#f1f1f1', minHeight: '600px'}}>
      <Index>
        <Routes>
          <Route path="/about" element={<About />} />
          <Route path='/dashboard' element={<Dashboard />} />
          <Route path='/side' element={<Sidebar />} />
          <Route path='/coa' element={<Coa/>}/>
          <Route path='/project' element={<Project/>}/>
          <Route path='/bank' element={<Bank/>}/>
          <Route path='/coaCategory' element={<CoaCategory/>}/>
          <Route path='/rekening' element={<Rekening />} />
          <Route path='/client' element={<Client />} />
          <Route path='/vendor' element={<Vendor/>}/>
          <Route path='/division' element={<Division />} />
          <Route path='/authRegister' element={<Register />} />
          <Route path='/rfpPetty' element={<RfpPettyCash/>}/>
          <Route path='/rfpUmum' element={<RfpUmum/>}/>
          <Route path='/rfpTransaksi' element={<RfpTransaksi/>} />
          <Route path='/rfpDetail/:idRFP' element={<RfpDetail/>} />
          <Route path='/user' element={<UserData/>} />
          <Route path='/role' element={<Role />} />
        </Routes>
      </Index>
      <Routes>
      <Route path='/login' element={<Login/>}/>
      </Routes>
    </div>
  );
}

export default App;
