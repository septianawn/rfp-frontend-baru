import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component';
import { Button, Col, Container, Row } from 'react-bootstrap'
import FormUser from './FormUser';
// import CardComponent from '../../component/CardComponent';
// import BreadcrumbComponent from '../../component/BreadcrumbComponent';
import { ButtonPrimary } from '../../component/Button';
import * as BsIcons from "react-icons/bs";
import BreadcrumbComponent from '../../component/BreadcrumbComponent';
import CardComponent from '../../component/CardComponent';

const UserData = () => {
    const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
    const [data, setData] = useState()
    const columns = [
        {
            name: 'Username',
            selector: row => row.id,
            sortable: true,
        },
        {
            name: 'Password',
            selector: row => row.name,
            sortable: true,
        },
        {
            name: 'Name',
            selector: row => row.descr,
            sortable: true,
        },
        {
            name: 'Role',
            cell: row => (
                <>
                <Button color = 'secondary' onClick={(e)=> openModal(e, row.id, 'edit')}> Edit </Button>
                <Button color = 'danger' onClick={(e)=> openModal(e, row.id, 'delete')}> Delete </Button>
                </>
            )
        }
    ];

    
    const getApiData = async () => {
    const respone = await fetch("http://10.200.0.44:8080/api/division", { 
        method: 'get', 
        headers: new Headers({
    
            'Authorization': 'Bearer ' + localStorage.getItem('token'), 
            'Accept' : 'application/json',
            'Content-Type' : 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials' : 'true',
        })
    })
        .then((respone) => respone.json());
        setData(respone)
    };
    const close = () => setShowModal(!showModal)
    const alert = () => setMessage(!message)
    const [message, setMessage] = useState(false)
    const [showModal, setShowModal] = useState(false)
    const [stateForm, setStaeForm] = useState('')
    const [idData, setIdData] = useState('')

    const openModal = (e, id, state) => {
        e.preventDefault();
        setShowModal(true);
        setStaeForm(state);
        setIdData(id);
    }

    useEffect(() => {
        getApiData()
    }, [showModal]);

    return (

        <Container style={{ paddingTop: '100px' }}>
            <h5 style={{fontWeight: 'bold'}}>User</h5>
            <BreadcrumbComponent page={[{ path: '/UserData', name: 'UserData' }]} />
            <CardComponent>
                {/* <Card.Body> */}
                <Row>
                    <Col md={6} style={{ textAlign: 'left' }}>
                        <h6>Daftar User</h6>
                    </Col>
                    <Col md={6} style={{ textAlign: 'right' }}>
                        <ButtonPrimary onClick={(e) => openModal(e, '', 'add', getApiData())}> 
                        <BsIcons.BsPlusCircle/> Add Data</ButtonPrimary>
                    </Col>
                    {message ? (
                    <Col md={12}>
                        <p>{stateForm} data berhasil</p>
                    </Col>) : null}
                    <Col md={12}>

                        <DataTable
                            columns={columns}
                            data={data}
                            //  progressPending={pending}  
                            expandableRows pagination
                            expandableRowsComponent={ExpandedComponent}
                        />
                    </Col>
                </Row>
                {/* </Card.Body> */}
            </CardComponent>
            <FormUser
                show={showModal}
                close={close}
                alert={alert}
                state={stateForm}
                id={idData}
            />
        </Container>
    );
}

export default UserData