import React, { useEffect, useState } from 'react'
import { Button, Col, Dropdown, Modal, Row } from 'react-bootstrap';

const FormUser = ({  show=false,close, state, id,alert  }) => {
  const [tokens, setTokens] = useState(localStorage.getItem('token'))
  const [userName, setuserName] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [dataRoles, setDataRoles] = useState([])
  
  const token = localStorage.getItem('token')
  
  const saveData = () =>{
        
  }
  // const [role, setRole] = useState("");

  //define state validation
  const [validation, setValidation] = useState([]);


  //function "registerHanlder"
  const registerHandler = async (e) => {
    e.preventDefault();

    //initialize formData
    const formData = new FormData();

    //append data to formData
    formData.append('username', userName);
    formData.append('password', password);
    formData.append('name', name);
    // formData.append('role', role);

    const _submit = async (e) => {
      // agar tidak perlu load form login saat submit
      e.preventDefault();
      // menghubungkan ke be dgn api
      const response = await fetch("http://10.200.0.44:8080/api/auth/register", {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          userName: userName,
          password: password,
          name: name,
          // role: role
        }),
      });
      // kondisi jika respon di be 200, maka akan ditampilkan pesan di console
      if (response.status === 200) {
        const json = await response.json();
        console.log("Anda berhasil Register")
        token(json.token)
        localStorage.setItem("token", json.token);
      }
      else {
        console.log("Gagal Register")
        token(null)
      }
    }
  };
  const getRole = async () => {
    const response = await fetch("http://10.200.0.44:8080/api/RoleRfp", {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + tokens
      },
    });
    if (response.status === 200) {
      const dataRole = await response.json();
      setDataRoles(dataRole)
    }
  }

  useEffect(() => {
    getRole();
  }, [])

  return (
    <>
      <Modal show={show} onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title>{state} Register </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col md={12} style={{ marginBottom: '19px' }}>
              <div>
                <label className="form-label">username</label>
                <input type="text" className="form-control" value={userName} onChange={(e) => setuserName(e.target.value)} placeholder="Masukkan Username" />
              </div>
              {
                validation.userName && (
                  <div className="alert alert-danger">
                    {validation.userName[0]}
                  </div>
                )
              }
            </Col>
            <Col md={12} style={{ marginBottom: '19px' }}>
              <div>
                <div className="col-md-6">
                  <div className="mb-3">
                    <label className="form-label">password</label>
                    <input type="password" className="form-control" value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Masukkan Password" />
                  </div>
                  {
                    validation.password && (
                      <div className="alert alert-danger">
                        {validation.password[0]}
                      </div>
                    )
                  }
                  </div>
                  </div>
                </Col>
                <Col md={12} style={{ marginBottom: '19px' }}>
                  <div>
                    <div className="mb-3">
                      <label className="form-label">name</label>
                      <input type="name" className="form-control" value={name} onChange={(e) => setName(e.target.value)} placeholder="Masukkan Nama Lengkap" />
                    </div>
                    </div>
                    {
                      validation.name && (
                        <div className="alert alert-danger">
                          {validation.name[0]}
                        </div>
                      )
                    }
                </Col>
                <Col md={12} style={{ marginBottom: '19px' }}>
                  <div>
                    <label className="form-label">role</label>
                    <Dropdown>
                      <Dropdown.Toggle variant="success" id="dropdown-basic">
                        Pilih Role
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        {dataRoles.map((item) => (

                          <Dropdown.Item href="#/action-1">{item.name}</Dropdown.Item>
                        )
                        )}
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                </Col>
              </Row>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={close}>
                Close
              </Button>
              <Button variant="primary" onClick={saveData()}>
                Save Changes
              </Button>
            </Modal.Footer>
          </Modal>
        </>
        )
}

        export default FormUser