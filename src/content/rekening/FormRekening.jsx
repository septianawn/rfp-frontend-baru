import React, { useEffect, useState } from 'react'
import { Button, Col, Form, Modal, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { getBankList } from '../../redux/action/BankAction';
import { deleteRekening, editRekening, postRekening } from '../../redux/action/RekeningAction';
import { toggleModal } from '../../redux/slice/RekeningSlice';


const FormRekening = ({ show = false, close, state, alert, id, getApi }) => { 
  const dispatch = useDispatch()
  const {dataById} = useSelector((state)=> state.rekening)
  const {dataBank} = useSelector((state)=> state.bank)
  const [rek, setRek] = useState('');
  const [name, setName] = useState('');
  const [descr, setDescr] = useState('');
  const [bankId, setBankId] = useState('');
  const [isCompany, setIsCompany] = useState('');
  const [validation, setValidation] = useState([
    {noRek : false , nameRek: false, desc: false, codeBank: false}
  ]);
  const param = {}
  useEffect(() => {
    dispatch(getBankList())
  }, [])
  const FormData = () => {
    if (state === 'add') {
      return (
        <Row>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Nomor Rekening </label>
            <input style={{border: (validation[0].noRek ? '1px solid red' : '1px solid #ced4da')}} type="text" defaultValue={id} className='form-control'
              onChange={(e) => setRek(e.target.value)} />
              {validation[0].noRek ? (<span style={{color: 'red'}}>Nomor Rekening Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Nama Rekening </label>
            <input style={{border: (validation[0].namaRek ? '1px solid red' : '1px solid #ced4da')}} type="text" defaultValue={name} className='form-control'
              onChange={(e) => setName(e.target.value)} />
              {validation[0].namaRek ? (<span style={{color: 'red'}}>Nama Rekening Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Deskripsi </label>
            <input style={{border: (validation[0].desc ? '1px solid red' : '1px solid #ced4da')}} type="text" defaultValue={descr} className='form-control'
              onChange={(e) => setDescr(e.target.value)} />
              {validation[0].desc ? (<span style={{color: 'red'}}>Deskripsi Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
              <label> Rekening Perusahaan </label>
              <Form style={{border: (validation[0].isCompany ? '1px solid red' : '1px solid #ced4da')}}
              onChange={(e) => setIsCompany(e.target.value)}>
              {['radio'].map((type) => (
              <div key={`inline-${type}`} className="mb-3">
              <Form.Check
                inline label="True"
                value={true}
                name="isCompany" type={type}
              />
              <Form.Check
                inline label="false"
                value={false}
                name="isCompany" type={type}
              />
              </div>
              ))}
              </Form>
              {validation[0].isCompany ? (<span style={{color: 'red'}}> Data Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Nama Bank </label>
            <Form.Select style={{border: (validation[0].codeBank ? '1px solid red' : '1px solid #ced4da')}} onChange={(e) => setBankId(e.target.value)}>
              <option value="" selected disabled>--Pilih Bank--</option>
              {dataBank.map((item) => (
                  <option value={item.id}>{item.name}</option>
                )
              )}
            </Form.Select>
            {validation[0].codeBank ? (<span style={{color: 'red'}}>Bank Tidak Boleh Kosong</span>) : null}
          </Col>
        </Row>
      )
    } if (state === 'edit') {
      return (
        <Row>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Nama Rekening </label>
            <input style={{border: (validation[0].namaRek ? '1px solid red' : '1px solid #ced4da')}} type="text" defaultValue={dataById.name} className='form-control'
              onChange={(e) => setName(e.target.value)} />
              {validation[0].namaRek ? (<span style={{color: 'red'}}>Nama Rekening Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Deskripsi </label>
            <input style={{border: (validation[0].desc ? '1px solid red' : '1px solid #ced4da')}} type="text" defaultValue={dataById.descr} className='form-control'
              onChange={(e) => setDescr(e.target.value)} />
              {validation[0].desc ? (<span style={{color: 'red'}}>Deskripsi Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
              <label> Rekening Perusahaan </label>
              <Form style={{border: (validation[0].isCompany ? '1px solid red' : '1px solid #ced4da')}}
              onChange={(e) => setIsCompany(e.target.value)}>
              {['radio'].map((type) => (
              <div key={`inline-${type}`} className="mb-3">
              <Form.Check
                inline label="True"
                value={true}
                name="isCompany" type={type}
              />
              <Form.Check
                inline label="false"
                value={false}
                name="isCompany" type={type}
              />
              </div>
              ))}
              </Form>
              {validation[0].isCompany ? (<span style={{color: 'red'}}> Data Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Nama Bank </label>
            <Form.Select style={{border: (validation[0].codeBank ? '1px solid red' : '1px solid #ced4da')}} onChange={(e) => setBankId(e.target.value)}>
              <option value="" selected disabled>--Pilih Bank--</option>
              {dataBank.map((item) => (
                  <option value={item.id}>{item.name}</option>
                )
              )}
            </Form.Select>
            {validation[0].codeBank ? (<span style={{color: 'red'}}>Bank Tidak Boleh Kosong</span>) : null}
          </Col>
        </Row>
      )
    }
     else {
      return 'Yakin Ingin Menghapus?'
    }
  }
  const saveData = async event => {
    let body = {
      name: name,
      descr: descr,
      isCompany: isCompany,
      bankId: parseInt(bankId)
    }
    param.data = body
    event.preventDefault();
    if (state === 'add') {
      let noRek = rek;
      let namaRek = name;
      let desc = descr;
      let codeBank = bankId;
      let validate = [...validation]
      let validateStatus = false;
      if(noRek === ''){
        validate[0].noRek = true
        validateStatus = true
      }
      else{
        validate[0].noRek = false
        validateStatus = false
      }
      if(namaRek === ''){
        validate[0].namaRek = true
        validateStatus = true
      }
      else{
        validate[0].namaRek = false
        validateStatus = false
      }
      if(desc === ''){
        validate[0].desc = true
        validateStatus = true
      }
      else{
        validate[0].desc = false
        validateStatus = false
      }
      if(codeBank === ''){
        validate[0].codeBank = true
        validateStatus = true
      }
      else{
        validate[0].codeBank = false
        validateStatus = false
      }
      // console.log(validation)
      setValidation(validate)
      //api add
      if(validateStatus === false){
        param.data.id = rek
        dispatch(postRekening(param))
      }
    } else if (state === 'delete') {
      param.id = dataById.id
      dispatch(deleteRekening(param))
    } else {
      //api update
      param.id = dataById.id
      dispatch(editRekening(param))
    }
  }

  return (
    <>
      <Modal show={show} onHide={(e) => dispatch(toggleModal({ state: 'add' }))}>
        <Modal.Header closeButton>
          <Modal.Title>{state} Rekening </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {FormData()}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={(e) => dispatch(toggleModal({ state: 'add' }))}>
            Close
          </Button>
          <Button variant="primary" onClick={saveData}>
            Save Data
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default FormRekening
