import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component';
import { Col, Container, Row } from 'react-bootstrap'
import Spinner from 'react-bootstrap/Spinner';
import FormRekening from './FormRekening';
import { ButtonPrimary } from '../../component/Button';
import CardComponent from '../../component/CardComponent';
import BreadcrumbComponent from '../../component/BreadcrumbComponent';
import * as BsIcons from "react-icons/bs";
import { useDispatch, useSelector } from 'react-redux';
import { toggleModal } from '../../redux/slice/RekeningSlice';
import { getRekeningList } from '../../redux/action/RekeningAction';

const Rekening = () => {
    const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
    const dispatch = useDispatch()
    const {dataRekening,isLoading,isOpen,state} = useSelector((state)=> state.rekening)
    const [data, setData] = useState()
    const [loading, setLoading] = useState(false);
    const columns = [ 
        {
            name: 'No',
            cell: (row, index) => index + 1,
            sortable: true,
            width:'8%',
        },
        {
            name: 'Nomor Rekening',
            selector: row => row.id,
            sortable: true,
            width:'auto',
        },
        {
            name: 'Nama Rekening',
            selector: row => row.name,
            sortable: true,
            width:'auto',
        },
        {
            name: 'Deskripsi',
            selector: row => row.descr,
            sortable: true,
            width:'auto',
        },
        {
            name: 'Nama Bank',
            selector: row => row.detailBank.name,
            sortable: true,
            width:'auto',
        },
        {
            name: 'Action',
            cell: row => (
                <>
                    <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'edit', data: row, id: row.id}))}> <BsIcons.BsPencil /></ButtonPrimary>
                    <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'delete', data: row, id: row.id }))}> <BsIcons.BsTrash /></ButtonPrimary>
                </>
            )
        }
    ];

    const [showModal, setShowModal] = useState(false)
    const alert = () => setMessage(!message)
    const [message, setMessage] = useState(false)
    const [stateForm, setStaeForm] = useState('')
    const [idData, setIdData] = useState('')
    const openModal = (e, id, state) => {
        e.preventDefault();
        setShowModal(true);
        setStaeForm(state);
        setIdData(id);
    }

    useEffect(() => {
        dispatch(getRekeningList())
    }, []);

    return (

        <Container style={{ padding: '100px 0' }}>
            <h5 style={{ fontWeight: 'bold' }}>Rekening</h5>
            <BreadcrumbComponent page={[{ path: '/rekening', name: 'Rekening' }]} />
            <CardComponent>
                {/* <Card.Body> */}
                <Row>
                    <Col md={6} style={{ textAlign: 'left' }}>
                        <h6>Daftar Rekening</h6>
                    </Col>
                    <Col md={6} style={{ textAlign: 'right' }}>
                        <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'add' }))}>
                            <BsIcons.BsPlusCircle /> Add Data</ButtonPrimary>
                    </Col>
                    {message ? (
                        <Col md={12}>
                            <p>{stateForm} data berhasil</p>
                        </Col>) : null}
                    <Col md={12} className="text-center">
                        
                        { isLoading ? <Spinner animation="border" /> :
                            <DataTable
                                columns={columns}
                                data={dataRekening}
                                //  progressPending={pending}  
                                pagination
                                expandableRowsComponent={ExpandedComponent}
                            />
                        }
                    </Col>
                </Row>
                {/* </Card.Body> */}
            </CardComponent>
            <FormRekening
                show={isOpen}
                close={setShowModal}
                alert={alert}
                state={state}
                id={idData}
            />
        </Container>
    );
}

export default Rekening