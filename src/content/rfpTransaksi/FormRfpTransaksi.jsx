import React, { useEffect, useState } from 'react'
import { Button, Col, Modal, Row } from 'react-bootstrap';
import ReactDatePicker from 'react-datepicker';

const FormRfpTransaksi = ({ show = false, close, state, alert, id, getApi }) => {
  const [transaksiDate, setTransaksiDate] = useState('');
  const [uraian, setUraian] = useState('');
  const [penerimaan, setPenerimaan] = useState('');
  const [qty, setQty] = useState('');
  const [harga, setHarga] = useState('');
  const [pengeluaran, setPengeluaran] = useState('');
  const [bukti, setBukti] = useState('');
  const [validation, setValidation] = useState([
    {
      transaksiTgl: false, urai: false, penerima: false, jumlah: false, price: false,
      pengeluar: false, proof: false
    }
  ]);
  const getById = async () => {
    if (state === 'edit') {
      const requestOptions = {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + localStorage.getItem('token'),
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': 'true'
        },
        // body: JSON.stringify({ name : name })
      };
      await fetch('http://10.200.0.44:8080/api/rfp-detail/' + id, requestOptions)
        .then(response => response.json())
        .then(data => {
          // setIdDetail(data.idDetail)
          setTransaksiDate(data.transaksiDate)
          setUraian(data.uraian)
          setPenerimaan(data.penerimaan)
          setQty(data.qty)
          setHarga(data.harga)
          setPengeluaran(data.pengeluaran)
          setBukti(data.bukti)
        })
    }
  }
  const FormData = () => {
    if (state === 'add') {
      //api add
      return (
        <Row>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Tanggal Rfp </label>
            <ReactDatePicker
              selected={transaksiDate} dateFormat="yyyy-MM-dd" timeFormat='HH:mm'
              onChange={(date) => { const d = new Date(date); setTransaksiDate(d); }} style={{ border: (validation[0].transaksiDate ? '1px solid red' : '1px solid #ced4da') }} className='form-control' />
            {validation[0].tanggal ? (<span style={{ color: 'red' }}>Tanggal Rfp Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> ACC Number </label>
            <input style={{ border: (validation[0].urai ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={uraian} className='form-control'
              onChange={(e) => setUraian(e.target.value)} />
            {validation[0].urai ? (<span style={{ color: 'red' }}>Coa Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Rekening Tujuan </label>
            <input style={{ border: (validation[0].penerima ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={penerimaan} className='form-control'
              onChange={(e) => setPenerimaan(e.target.value)} />
            {validation[0].penerima ? (<span style={{ color: 'red' }}>Rekening Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Total  </label>
            <input style={{ border: (validation[0].jumlah ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={qty} className='form-control'
              onChange={(e) => setQty(e.target.value)} />
            {validation[0].jumlah ? (<span style={{ color: 'red' }}>Total Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Tangga Pembayaran  </label>
            <input style={{ border: (validation[0].price ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={harga} className='form-control'
              onChange={(e) => setHarga(e.target.value)} />
            {validation[0].price ? (<span style={{ color: 'red' }}>Total Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Tangga Pembayaran  </label>
            <input style={{ border: (validation[0].pengeluar ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={pengeluaran} className='form-control'
              onChange={(e) => setPengeluaran(e.target.value)} />
            {validation[0].pengeluar ? (<span style={{ color: 'red' }}>Total Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> bukti </label>
            <input style={{ border: (validation[0].proof ? '1px solid red' : '1px solid #ced4da') }} type="file" defaultValue={bukti} className='form-control'
              onChange={(e) => setBukti(e.target.value)} />
            {validation[0].proof ? (<span style={{ color: 'red' }}>bukti Tidak Boleh Kosong</span>) : null}
          </Col>
        </Row>
      )
    } else {
      return 'yakin ingin dihapus?'
    }
  }
  const saveData = async event => {
    event.preventDefault();
    if (state === 'add') {
      let transaksiTgl = transaksiDate;
      let urai = uraian;
      let penerima = penerimaan;
      let jumlah = qty;
      let price = harga;
      let pengeluar = pengeluaran;
      let proof = bukti;
      let validate = [...validation]
      let validateStatus = false;
      if (transaksiTgl === '') {
        validate[0].transaksiTgl = true
        validateStatus = true
      }
      else {
        validate[0].transaksiTgl = false
        validateStatus = false
      }
      if (urai === '') {
        validate[0].urai = true
        validateStatus = true
      }
      else {
        validate[0].urai = false
        validateStatus = false
      }
      if (penerima === '') {
        validate[0].penerima = true
        validateStatus = true
      }
      else {
        validate[0].penerima = false
        validateStatus = false
      }
      if (jumlah === '') {
        validate[0].jumlah = true
        validateStatus = true
      }
      else {
        validate[0].jumlah = false
        validateStatus = false
      }
      if (price === '') {
        validate[0].price = true
        validateStatus = true
      }
      else {
        validate[0].price = false
        validateStatus = false
      }
      if (proof === '') {
        validate[0].proof = true
        validateStatus = true
      }
      else {
        validate[0].proof = false
        validateStatus = false
      }
      if (pengeluar === '') {
        validate[0].pengeluar = true
        validateStatus = true
      }
      else {
        validate[0].pengeluar = false
        validateStatus = false
      }
      if (proof === '') {
        validate[0].proof = true
        validateStatus = true
      }
      else {
        validate[0].proof = false
        validateStatus = false
      }
      setValidation(validate)
      if (validateStatus === false) {
        const requestOptions = {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': 'true'
          },
          body: JSON.stringify({
            transaksiDate: transaksiDate,
            uraian: uraian,
            penerimaan: penerimaan,
            qty: qty,
            harga: harga,
            pengeluaran: pengeluaran,
            bukti: bukti
          })
        };
        fetch('http://10.200.0.44:8080/api/rfp-transaksi/', requestOptions)
          .then(response => response.json())
          .then(close(!show))
          .then(alert)
      }
    } else if (state === 'delete') {
      const requestOptions = {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + localStorage.getItem('token'),
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': 'true'
        },
      };
      fetch(`http://10.200.0.44:8080/api/rfp-transaksi/${id}`, requestOptions)
        .then(response => response.json())
        .then(close(!show))
        .then(alert)
    } else {
      //api update
      const requestOptions = {
        method: 'PUT',
        headers: {
          'Authorization': 'Bearer ' + localStorage.getItem('token'),
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': 'true'
        },
        body: JSON.stringify({
          transaksiDate: transaksiDate,
          uraian: uraian,
          penerimaan: penerimaan,
          qty: qty,
          harga: harga,
          pengeluaran: pengeluaran,
          bukti: bukti,
        })
      };
      fetch(`http://10.200.0.44:8080/api/rfp-detail/%{id}`, requestOptions)
        .then(response => response.json())
        .then(close(!show))
        .then(alert)
    }
  }
  useEffect(() => {
    getById();
  }, [show])


  return (
    <>
      <Modal show={show} onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title>{state} RFP Transaksi </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {FormData()}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={close}>
            Close
          </Button>
          <Button variant="primary" onClick={saveData}>
            Save Data
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default FormRfpTransaksi
