import React, { useEffect, useState } from 'react'
import { Button, Col, Form, Modal, Row } from 'react-bootstrap';

const FormProject = ({show=false, close, state,alert,id,getApi}) => {
    // const [dataById, setDataById] = useState([]);
    const [pro, setPro] = useState('');    
    const [name, setName] = useState('');
    const [descr, setDescr] = useState('');
    const [status, setStatus] = useState(''); 
    const [clientId, setClientId] = useState('');
    const [clientList, setClientList] = useState([]);
    const [validation, setValidation] = useState([
      {idPro : false , namePro: false, desc: false, statusPro: false,codeClient: false}
    ]);
    const getById = async () => {
    if(state==='edit'){
      const requestOptions = {
        method: 'GET',
        headers: { 
          'Authorization': 'Bearer ' + localStorage.getItem('token'), 
            'Accept' : 'application/json',
            'Content-Type' : 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials' : 'true' 
          },
            // body: JSON.stringify({ name : name })
        };
        fetch('http://10.200.0.44:8080/api/project/'+id, requestOptions)
            .then(response => response.json())
            .then(data => {
              setPro(data.pro)
              setName(data.name)
              setDescr(data.descr)
              setStatus(data.status)
              setClientId(data.clientId)
            })
    }
  }
  useEffect(() => {
    fetch('http://10.200.0.44:8080/api/client', {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      }
    })
      .then(response => response.json())
      .then(response => setClientList(response))
  }, [])
  const FormData = () =>{
    if(state==='add' || state==='edit'){
      return (
        <Row>
            {/* <Col md={12} style={{marginBottom: '19px'}}>
                    <label> ID Project </label> 
                    <input style={{border: (validation[0].idPro ? '1px solid red' : '1px solid #ced4da')}} type="text" defaultValue={id} className='form-control' 
                    onChange={(e)=>setPro(e.target.value)} />
                    {validation[0].idPro ? (<span style={{color: 'red'}}>ID Project Tidak Boleh Kosong</span>) : null}
                </Col> */}
                <Col md={12} style={{marginBottom: '19px'}}>
                    <label> Nama Project </label> 
                    <input style={{border: (validation[0].namePro ? '1px solid red' : '1px solid #ced4da')}}type="text" defaultValue={name} className='form-control' 
                    onChange={(e)=>setName(e.target.value)} />
                    {validation[0].namePro ? (<span style={{color: 'red'}}>Nama Project Tidak Boleh Kosong</span>) : null}
                </Col>
                <Col md={12} style={{marginBottom: '19px'}}>
                    <label> Deskripsi </label> 
                    <input style={{border: (validation[0].desc ? '1px solid red' : '1px solid #ced4da')}}type="text" defaultValue={descr} className='form-control' 
                    onChange={(e)=>setDescr(e.target.value)} />
                    {validation[0].desc ? (<span style={{color: 'red'}}>Deskripsi Tidak Boleh Kosong</span>) : null}
                </Col>
                <Col md={12} style={{marginBottom: '19px'}}>
                    <label> Status Project </label> 
                    <input style={{border: (validation[0].statusPro ? '1px solid red' : '1px solid #ced4da')}}type="text" defaultValue={status} className='form-control' 
                    onChange={(e)=>setStatus(e.target.value)} />
                    {validation[0].statusPro ? (<span style={{color: 'red'}}>Status Tidak Boleh Kosong</span>) : null}
                </Col>
                <Col md={12} style={{marginBottom: '19px'}}>
                    <label> Nama Client </label>
                    <Form.Select style={{border: (validation[0].codeClient ? '1px solid red' : '1px solid #ced4da')}} onChange={(e) => setClientId(e.target.value)}>
                    <option value="" selected disabled>--Pilih Client--</option>
                    {clientList.map((item) => (
                    <option value={item.id}>{item.name}</option>
                    )
                  )}
                </Form.Select>
                    {validation[0].codeClient ? (<span style={{color: 'red'}}>Client Tidak Boleh Kosong</span>) : null}                
                </Col>
            </Row>
      )
    }else{
      return 'Yakin Ingin Menghapus?'
    }
  }
    const saveData = async event =>{
      event.preventDefault();
        if(state==='add'){
          let idPro = pro;
          let namePro = name;
          let desc = descr;
          let statusPro = status;
          let codeClient = clientId;
          let validate = [...validation]
          let validateStatus = false;
          if(idPro === ''){
            validate[0].idPro = true
            validateStatus = true
          }
          else{
            validate[0].idPro = false
            validateStatus = false
          }
          if(namePro === ''){
            validate[0].namePro = true
            validateStatus = true
          }
          else{
            validate[0].namePro = false
            validateStatus = false
          }
          if(desc === ''){
            validate[0].desc = true
            validateStatus = true
          }
          else{
            validate[0].desc = false
            validateStatus = false
          }
          if(statusPro === ''){
            validate[0].statusPro = true
            validateStatus = true
          }
          else{
            validate[0].statusPro = false
            validateStatus = false
          }
          if(codeClient === ''){
            validate[0].codeClient = true
            validateStatus = true
          }
          else{
            validate[0].codeClient = false
            validateStatus = false
          }
          setValidation(validate)
          if(validateStatus === false){
            //api add
            const requestOptions = {
              method: 'POST',
              headers: { 
                'Authorization': 'Bearer ' + localStorage.getItem('token'), 
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials' : 'true' 
              },
              body: JSON.stringify({ 
                id : pro,
                name : name,
                descr : descr,
                status : status,
                clientId: clientId
              })
          };
          fetch('http://10.200.0.44:8080/api/project/', requestOptions)
              .then(response => response.json())
              .then(close(!show))
              .then(alert)
        }
          }else if(state==='delete'){
            const requestOptions = {
              method: 'DELETE',
              headers: { 
                'Authorization': 'Bearer ' + localStorage.getItem('token'), 
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials' : 'true' 
              },
          };
          fetch(`http://10.200.0.44:8080/api/project/${id}`, requestOptions)
              .then(response => response.json())
              .then(close(!show))
              .then(alert)
          }else{
            //api update
            const requestOptions = {
              method: 'POST',
              headers: { 
                'Authorization': 'Bearer ' + localStorage.getItem('token'), 
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials' : 'true' 
              },
              body: JSON.stringify({ 
                id : pro,
                name : name,
                descr : descr,
                status : status,
                clientId: clientId 
                  })
          };
          fetch(`http://10.200.0.44:8080/api/project/${id}`, requestOptions)
              .then(response => response.json())
              .then(close(!show))
              .then(alert)
        }
    }
    useEffect(()=>{
      getById();
    },[show]);
  
    return (
      <>
        <Modal show={show} onHide={close}>
          <Modal.Header closeButton>
            <Modal.Title>{state} Project </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {/* <Row>
            <Col md={12} style={{marginBottom: '19px'}}>
                    <label> Nomor Rekening </label> 
                    <input type="text" defaultValue={id} className='form-control' 
                    onChange={(e)=>setRek(e.target.value)} />
                </Col>
                <Col md={12} style={{marginBottom: '19px'}}>
                    <label> Nama Rekening </label> 
                    <input type="text" defaultValue={name} className='form-control' 
                    onChange={(e)=>setName(e.target.value)} />
                </Col>
                <Col md={12} style={{marginBottom: '19px'}}>
                    <label> Deskripsi </label> 
                    <input type="text" defaultValue={descr} className='form-control' 
                    onChange={(e)=>setDescr(e.target.value)} />
                </Col>
                <Col md={12} style={{marginBottom: '19px'}}>
                    <label for="id_bank" class="col-sm-2 col-form-label">Rekening Perusahaan</label>
                    <div class="col-sm-10">
                    <select name="id_bank" id="id_bank">
                    <option selected disabled> -- Choose Kode Bank -- </option>
                    </select></div>
                </Col>
            </Row> */}
            {FormData()}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={close}>
              Close
            </Button>
            <Button variant="primary" onClick={saveData}>
              Save Data
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    )
}

export default FormProject
