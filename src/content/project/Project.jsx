import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component';
import { Col, Container, Row, Spinner } from 'react-bootstrap'
import { ButtonPrimary } from '../../component/Button';
import CardComponent from '../../component/CardComponent';
import BreadcrumbComponent from '../../component/BreadcrumbComponent';
import * as BsIcons from "react-icons/bs";
import FormProject from './FormProject';

const Project = () => {
    const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
    const [data, setData] = useState()
    const [loading, setLoading] = useState(false);  
    const columns = [
        {
            name: 'No',
            cell: (row, index) => index + 1,
            width:'8%',
        },
        {
            name: 'Nama Project',
            selector: row => row.name,
            sortable: true,
            width:'auto',
        },
        {
            name: 'Deskripsi',
            selector: row => row.descr,
            sortable: true,
            width:'auto',
        },
        {
          name: 'Status Project',
          selector: row => row.status,
          sortable: true,
          width:'auto',
      },
        {
            name: 'Client',
            selector: row => row.detailClient.name,
            sortable: true,
        },
        {
            name: 'Action',
            cell : row => (
                <>
                <ButtonPrimary onClick={(e)=> openModal(e, row.id, 'edit')}> <BsIcons.BsPencil/></ButtonPrimary> 
                <ButtonPrimary onClick={(e)=> openModal(e, row.id, 'delete')}> <BsIcons.BsTrash/></ButtonPrimary>
                </>
            )
        }
    ];

    const getApiData = async () => {
        setLoading(true);
        const respone = await fetch("http://10.200.0.44:8080/api/project", { 
            method: 'get', 
            headers: new Headers({
                'Authorization': 'Bearer ' + localStorage.getItem('token'), 
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials' : 'true',
            })
        })
        .then((respone) => respone.json());
        setData(respone)
        setLoading(false);
    };
    const [showModal, setShowModal] = useState(false)
    const alert = () => setMessage(!message)
    const [message, setMessage] = useState(false)
    const [stateForm, setStaeForm] = useState('')
    const [idData, setIdData] = useState('')
    const openModal = (e, id, state) => {
        e.preventDefault();
        setShowModal(true);
        setStaeForm(state);
        setIdData(id);
    }

    
    useEffect(() => {
        getApiData()
    }, [showModal]);

    return (
        
        <Container style={{ padding: '100px 0' }}>
            <h5 style={{fontWeight: 'bold'}}>Project</h5>
            <BreadcrumbComponent page={[{ path: '/project', name: 'Project' }]} />
            <CardComponent>
                {/* <Card.Body> */}
                <Row>
                    <Col md={6} style={{ textAlign: 'left' }}>
                        <h6>Daftar Project</h6>
                    </Col>
                    <Col md={6} style={{ textAlign: 'right' }}>
                        <ButtonPrimary onClick={(e) => openModal(e, '', 'add', getApiData())}> 
                        <BsIcons.BsPlusCircle/> Add Data</ButtonPrimary>
                    </Col>
                    {message ? (
                    <Col md={12}>
                        <p>{stateForm} data berhasil</p>
                    </Col>) : null}
                    <Col md={12} className="text-center">

                        {loading ? <Spinner animation="border" /> :
                            <DataTable
                                columns={columns}
                                data={data}
                                //  progressPending={pending}  
                                pagination
                                expandableRowsComponent={ExpandedComponent}
                            />
                        }
                        {loading ? <Spinner animation="border" /> :
                            <DataTable
                                columns1={columns}
                                data={data}
                                //  progressPending={pending}  
                                pagination
                                expandableRowsComponent={ExpandedComponent}
                            />
                        }

                    </Col>
                </Row>
                {/* </Card.Body> */}
            </CardComponent>
            <FormProject
                show={showModal}
                close={setShowModal}
                alert={alert}
                state={stateForm}
                id={idData}
            />
        </Container>
    );
}

export default Project