import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component';
import { Col, Container, Row, Spinner, } from 'react-bootstrap'
import FormClient from './FormClient';
import { ButtonPrimary } from '../../component/Button';
import BreadcrumbComponent from '../../component/BreadcrumbComponent';
import CardComponent from '../../component/CardComponent';
import * as BsIcons from "react-icons/bs";
import { useDispatch, useSelector } from 'react-redux';
import { toggleModal } from '../../redux/slice/ClientSlice';
import { getClientList } from '../../redux/action/ClientAction';

const Client = () => {
    const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
    const dispatch = useDispatch()
    const { dataClient, isOpen, state, isLoading } = useSelector((state) => state.client)
    const [loading, setLoading] = useState(false);
    const columns = [
        {
            name: 'No',
            cell: (row, index) => index + 1,
            width: '8%',
        },
        {
            name: 'Nama Client',
            selector: row => row.name,
            sortable: true,
            width: 'auto',
        },
        {
            name: 'Action',
            cell: row => (
                <>
                    <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'edit', data: row, id: row.id }))}> <BsIcons.BsPencil /></ButtonPrimary>
                    <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'delete', data: row, id: row.id }))}> <BsIcons.BsTrash /></ButtonPrimary>
                </>
            )
        }
    ];

    const [showModal, setShowModal] = useState(false)
    const alert = () => setMessage(!message)
    const [message, setMessage] = useState(false)
    const [stateForm, setStaeForm] = useState('')
    const [idData, setIdData] = useState('')
    const openModal = (e, id, state) => {
        e.preventDefault();
        setShowModal(true);
        setStaeForm(state);
        setIdData(id);
    }

    useEffect(() => {
        dispatch(getClientList())
    }, []);

    return (
        <Container style={{ padding: '100px 0' }}>
            <h5 style={{ fontWeight: 'bold' }}>Client</h5>
            <BreadcrumbComponent page={[{ path: '/client', name: 'Client' }]} />
            <CardComponent>
                {/* <Card.Body> */}
                <Row>
                    <Col md={6} style={{ textAlign: 'left' }}>
                        <h6>Daftar Client</h6>
                    </Col>
                    <Col md={6} style={{ textAlign: 'right' }}>
                        <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'add' }))}>
                            <BsIcons.BsPlusCircle /> Add Data</ButtonPrimary>
                    </Col>
                    {message ? (
                        <Col md={12}>
                            <p>{stateForm} data berhasil</p>
                        </Col>) : null}
                    <Col md={12} className="text-center">
                        {isLoading ? <Spinner animation="border" /> :
                            <DataTable
                                columns={columns}
                                data={dataClient}
                                //  progressPending={pending}  
                                pagination
                                expandableRowsComponent={ExpandedComponent}
                            />
                        }
                    </Col>
                </Row>
                {/* </Card.Body> */}
            </CardComponent>
            <FormClient
                show={isOpen}
                close={setShowModal}
                alert={alert}
                state={state}
                id={idData}
            />
        </Container>
    );
}

export default Client