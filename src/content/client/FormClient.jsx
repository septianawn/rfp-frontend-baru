import React, { useEffect, useState } from 'react'
import { Button, Col, Modal, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { deleteClient, editClient, postClient } from '../../redux/action/ClientAction';
import { toggleModal } from '../../redux/slice/ClientSlice';

const FormClient = ({ show = false, close, state, alert, id, getApi }) => {
  // const [dataById, setDataById] = useState([]);
  const dispatch = useDispatch()
  const { dataById } = useSelector((state) => state.client)
  const [name, setName] = useState('');
  const [validation, setValidation] = useState([
    { nameClient: false }
  ]);
  const param = {}

  const FormData = () => {
    if (state === 'add' || state === 'edit') {
      return (
        <Row>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Nama Client </label>
            <input style={{ border: (validation[0].nameClient ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={dataById.name} className='form-control'
              onChange={(e) => setName(e.target.value)} />
            {validation[0].nameClient ? (<span style={{ color: 'red' }}>Nama Client Tidak Boleh Kosong</span>) : null}
          </Col>
        </Row>
      )
    }
    else {
      return 'Yakin Ingin Menghapus?'
    }
  }
  const saveData = async event => {
    event.preventDefault();
    let body = {
      name: name
    }
    param.data = body
    if (state === 'add') {
      let nameClient = name;
      let validate = [...validation]
      let validateStatus = false;
      if (nameClient === '') {
        validate[0].nameClient = true
        validateStatus = true
      }
      else {
        validate[0].nameClient = false
        validateStatus = false
      }
      setValidation(validate)
      //api add
      if (validateStatus === false) {
        //api add
        dispatch(postClient(param))
      }
    } else if (state === 'delete') {
      param.id = dataById.id
      dispatch(deleteClient(param))
    } else {
      //api update
      param.id = dataById.id
      dispatch(editClient(param))
    }
  }

  return (
    <>
      <Modal show={show} onHide={(e) => dispatch(toggleModal({ state: 'add' }))}>
        <Modal.Header closeButton>
          <Modal.Title>{state} Client </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* <Row>
                <Col md={12} style={{marginBottom: '19px'}}>
                    <label> Nama CLient </label> 
                    <input type="text" defaultValue={name} className='form-control' onChange={(e)=>setName(e.target.value)} />
                </Col>
            </Row> */}
          {FormData()}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={(e) => dispatch(toggleModal({ state: 'add' }))}>
            Close
          </Button>
          <Button variant="primary" onClick={saveData}>
            Save Data
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default FormClient
