import React, { useEffect, useState } from 'react'
import { Button, Col, Modal, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { editBank, getBankList, postBank, deleteBank } from '../../redux/action/BankAction';
import { toggleModal } from '../../redux/slice/BankSlice';

const FormBank = ({ show = false, close, state, alert, id, getApi }) => {
  // const [dataById, setDataById] = useState([]); 
  const dispatch = useDispatch()
  const { dataById } = useSelector((state) => state.bank)
  const [kode, setKode] = useState('');
  const [name, setName] = useState('');
  const [validation, setValidation] = useState([
    { kodeBank: false, nameBank: false }
  ]);
  const param = {}
  // console.log(name); 
  // console.log(state); 
  const FormData = () => {
    if (state === 'add') {
      return (
        <Row>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Kode Bank </label>
            <input style={{ border: (validation[0].kodeBank ? '1px solid red' : '1px solid #ced4da') }} type="number" defaultValue={kode} className='form-control'
              onChange={(e) => setKode(e.target.value)} />
            {validation[0].kodeBank ? (<span style={{ color: 'red' }}>Kode Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Nama Bank </label>
            <input style={{ border: (validation[0].nameBank ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={name} className='form-control'
              onChange={(e) => setName(e.target.value)} />
            {validation[0].nameBank ? (<span style={{ color: 'red' }}>Nama Tidak Boleh Kosong</span>) : null}
          </Col>
        </Row>
      )
    }
    else if (state === 'edit') {
      return (
        <Row>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Nama Bank </label>
            <input style={{ border: (validation[0].nameBank ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={dataById.name} className='form-control'
              onChange={(e) => setName(e.target.value)} />
            {validation[0].nameBank ? (<span style={{ color: 'red' }}>Nama Tidak Boleh Kosong</span>) : null}
          </Col>
        </Row>
      )
    }
    else {
      return 'Yakin Hapus?'
    }
  }
  const saveData = async event => {
    event.preventDefault();
    let body = {
      name: name
    }
    param.data = body
    if (state === 'add') {
      let kodeBank = kode;
      let nameBank = name;
      let validate = [...validation]
      let validateStatus = false;
      if (kodeBank === '') {
        validate[0].kodeBank = true
        validateStatus = true
      }
      else {
        validate[0].kodeBank = false
        validateStatus = false
      } if (nameBank === '') {
        validate[0].nameBank = true
        validateStatus = true
      }
      else {
        validate[0].nameBank = false
        validateStatus = false
      }
      setValidation(validate)
      if (validateStatus === false) {
        //api add 
        param.data.id = kode
        dispatch(postBank(param))
        // .then(getApi())
      }
    } else if (state === 'delete') {
      param.id = dataById.id
      dispatch(deleteBank(param))
    }
    else {
      param.id = dataById.id
      //api update 
      dispatch(editBank(param))
    }
  }

  return (
    <>
      <Modal show={show} onHide={(e) => dispatch(toggleModal({ state: 'add' }))}>
        <Modal.Header closeButton>
          <Modal.Title>{state} Bank </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {FormData()}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={(e) => dispatch(toggleModal({ state: 'add' }))}>
            Close
          </Button>
          <Button variant="primary" onClick={saveData}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default FormBank