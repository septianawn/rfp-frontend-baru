import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component';
import { Col, Container, Row, Spinner } from 'react-bootstrap'
import FormBank from './FormBank';
import CardComponent from '../../component/CardComponent';
import BreadcrumbComponent from '../../component/BreadcrumbComponent';
import { ButtonPrimary } from '../../component/Button';
import { useSelector, useDispatch } from 'react-redux'
import * as BsIcons from "react-icons/bs";
import { getBankList } from '../../redux/action/BankAction';
import { toggleModal } from '../../redux/slice/BankSlice';

const Bank = () => {
    const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
    const dispatch = useDispatch()
    const { dataBank, isOpen, state, isLoading } = useSelector((state) => state.bank)
    const [loading, setLoading] = useState(false);
    const columns = [
        {
            name: 'No',
            cell: (row, index) => index + 1,
            sortable: true,
            width: '8%'
        },
        {
            name: 'ID',
            selector: row => row.id,
            sortable: true,
            width: 'auto',
        },
        {
            name: 'Name',
            selector: row => row.name,
            sortable: true,
            width: 'auto',
        },
        {
            name: 'Action',
            cell: row => (
                <>
                    <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'edit', data: row, id: row.id }))}> <BsIcons.BsPencil /></ButtonPrimary>
                    <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'delete', data: row, id: row.id }))}> <BsIcons.BsTrash /></ButtonPrimary>
                </>
            )
        }
    ];


    const [showModal, setShowModal] = useState(false)
    const alert = () => setMessage(!message)
    const [message, setMessage] = useState(false)
    const [stateForm, setStateForm] = useState('')
    const [idData, setIdData] = useState('')
    const openModal = (e, id, state) => {
        e.preventDefault();
        setShowModal(true);
        setStateForm(state);
        setIdData(id);
    }
    useEffect(() => {
        dispatch(getBankList())
    }, []);

    return (

        <Container style={{ padding: '100px 0' }}>
            <h5 style={{ fontWeight: 'bold' }}>Bank</h5>
            <BreadcrumbComponent page={[{ path: '/bank', name: 'Bank' }]} />
            <CardComponent>
                {/* <Card.Body> */}
                <Row>
                    <Col md={6} style={{ textAlign: 'left' }}>
                        <h6>Daftar Bank</h6>
                    </Col>
                    <Col md={6} style={{ textAlign: 'right' }}>
                        <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'add' }))}>
                            <BsIcons.BsPlusCircle /> Add Data</ButtonPrimary>
                    </Col>
                    {message ? (
                        <Col md={12}>
                            <p>{stateForm} data berhasil</p>
                        </Col>) : null}
                    <Col md={12} className="text-center">
                        {isLoading ? <Spinner animation="border" /> :
                            <DataTable
                                columns={columns}
                                data={dataBank}
                                //  progressPending={pending}  
                                pagination
                                expandableRowsComponent={ExpandedComponent}
                            />
                        }
                    </Col>
                </Row>
            </CardComponent>
            <FormBank
                show={isOpen}
                close={setShowModal}
                alert={alert}
                state={state}
                id={idData}
            // getApi={getApiData()} 
            />
        </Container>
    );
}

export default Bank