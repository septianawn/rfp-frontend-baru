import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react';
import { Card, Col, Container, Row } from 'react-bootstrap'
import { Pie } from 'react-chartjs-2';
import { Bar } from 'react-chartjs-2';
import { Chart, registerables  } from 'chart.js';
import CardDashComponenet from '../component/CardDashComponenet'

Chart.register(...registerables);

const ContentStatus = ({ title, value }) => {
  return (
      <tr>
          <td style={{ width: '15%' }}>{title}</td>
          <td style={{ width: '3%' }}>:</td>
          <td style={{ width: '82%', fontWeight: 'bold' }}>{value}</td>
      </tr>
  )
}

const Dashboard = ({bgcolor}) => {
  const [dataCount, setDataCount]=useState()
  const [dataCountUm, setDataCountUm]=useState()
  const [dataPerm, setDataPerm]=useState()
  const [dataUmPerm, setDataUmPerm]=useState()
  const [dataStatus, setDataStatus]=useState()
  const [dataStatusCheck, setDataStatusCheck]=useState()
  const [dataStatusInp, setDataStatusInputed]=useState()
  const [dataStatusAck, setDataStatusAck]=useState()
  const [dataStatusApproved, setDataStatusApporved]=useState()
  const [loading, setLoading] = useState(false);
  const data = {
    labels:["Prepared", "Checked","Inputed","Acknowledged","Approved"],
      datasets: [
        {
          label: "Status Pengajuan RFP",
          data: [dataStatus, dataStatusCheck, dataStatusInp, dataStatusAck, dataStatusApproved],
          backgroundColor: ["pink", "orange", "blue", "red", "green"],
        }
      ]
  }
  const dataDiag = {
    labels: ["Januari", "Februari", "Maret"],
    datasets: [
      {
        id: 1,
        label: 'Petty Cash',
        data: [7, 6, 8],
        backgroundColor: "Red",
      },
      {
        id: 2,
        label: 'Umum',
        data: [6, 7, 8],
        backgroundColor: "Green",
      },
    ],
  }
  const getApiDataCount = async () => {
    const respone = await fetch("http://10.200.0.44:8080/api/rfp/countRfp?kategoriId=1" , {
        method: 'GET',
        headers: new Headers({
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': 'true',
        })
    })
        .then((respone) => respone.json());
        setDataCount(respone)
  };

  const getApiDataCountUm = async () => {
    const respone = await fetch("http://10.200.0.44:8080/api/rfp/countRfp?kategoriId=2" , {
        method: 'GET',
        headers: new Headers({
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': 'true',
        })
    })
        .then((respone) => respone.json());
        setDataCountUm(respone)
  };

  const getApiDataPerm = async () => {
    const respone = await fetch("http://10.200.0.44:8080/api/rfp/total?kategoriId=2" , {
        method: 'GET',
        headers: new Headers({
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': 'true',
        })
    })
        .then((respone) => respone.json());
        setDataPerm(respone)
  };

  const getApiDataUmPerm = async () => {
    const respone = await fetch("http://10.200.0.44:8080/api/rfp/total?kategoriId=1" , {
        method: 'GET',
        headers: new Headers({
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': 'true',
        })
    })
        .then((respone) => respone.json());
        setDataUmPerm(respone)
  };

  const getApiDataStatus = async () => {
    const respone = await fetch("http://10.200.0.44:8080/api/rfp/status/count?status=prepared" , {
        method: 'GET',
        headers: new Headers({
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': 'true',
        })
    })
        .then((respone) => respone.json());
        setDataStatus(respone)
  };

  const getApiDataStatusCheck = async () => {
    const respone = await fetch("http://10.200.0.44:8080/api/rfp/status/count?status=checked" , {
        method: 'GET',
        headers: new Headers({
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': 'true',
        })
    })
        .then((respone) => respone.json());
        setDataStatusCheck(respone)
  };

  const getApiDataStatusInp = async () => {
    const respone = await fetch("http://10.200.0.44:8080/api/rfp/status/count?status=inputed" , {
        method: 'GET',
        headers: new Headers({
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': 'true',
        })
    })
        .then((respone) => respone.json());
        setDataStatusInputed(respone)
  };

  const getApiDataStatusAck = async () => {
    const respone = await fetch("http://10.200.0.44:8080/api/rfp/status/count?status=acknowledged" , {
        method: 'GET',
        headers: new Headers({
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': 'true',
        })
    })
        .then((respone) => respone.json());
        setDataStatusAck(respone)
  };

  const getApiDataStatusApproved = async () => {
    const respone = await fetch("http://10.200.0.44:8080/api/rfp/status/count?status=approved" , {
        method: 'GET',
        headers: new Headers({
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': 'true',
        })
    })
        .then((respone) => respone.json());
        setDataStatusApporved(respone)
  };

  useEffect(() => {
    setLoading(true);
    getApiDataCount()
    getApiDataCountUm()
    getApiDataPerm()
    getApiDataUmPerm()
    getApiDataStatus()
    getApiDataStatusCheck()
    getApiDataStatusInp()
    getApiDataStatusAck()
    getApiDataStatusApproved()
    setLoading(false);
  },[])


  return (
    <Container style={{ padding: '100px 0' }}>
      <h5 style={{ fontWeight: 'bold', marginBottom: 20}}>Dashboard Overview</h5>
      <Row>
        <Col md={12}>
          <Row>
            <Col md={4}>
              <CardDashComponenet bgcolor='#4fb5ff' data={[{title: 'Jumlah Pengajuan',value: dataCount, valueUmum: dataCountUm}]} />
            </Col>

            <Col md={4}>
              <CardDashComponenet bgcolor='#82c43c' data={[{title: 'Nomimal Pengajuan', valueUmum: dataPerm, value: dataUmPerm}]}/>
            </Col>

            <Col md={4}>
              {/* <CardDashComponenet bgcolor='#fe974a' data={[{title: 'Status', value:dataStatus}]}/> */}
              <Card style={{background: '#fe974a', color: 'white', borderColor: bgcolor, borderRadius: '15px'}}>
                <Card.Body>
                    <table className="table table-borderless">
                        <tbody>
                            <ContentStatus title="Prepared" value={dataStatus} />
                            <ContentStatus title="Checked" value={dataStatusCheck} />
                            <ContentStatus title="Inputed" value={dataStatusInp} />
                            <ContentStatus title="Acknowledged" value={dataStatusAck} />
                            <ContentStatus title="Approved" value={dataStatusApproved} />
                        </tbody>
                    </table>
                  </Card.Body>
              </Card>
            </Col>

            <Col md={6} style={{align: 'center',paddingTop: 25}}>
              <Row>
                <Col xs={12} sm={12} md={12} lg={12}>
                    <span style={{fontSize: '15px', fontWeight: "bold"}}> Status Pengajuan RFP </span>
                </Col>
                <Col style={{alignSelf: 'center', padding: 0}}>
                    <Pie data={data} style={{width: 'auto'}} options={{responsive: true}}/>
                </Col> 
              </Row>
            </Col>

            <Col md={6} style={{align: 'center',paddingTop: 25}}>
            <Row>
                <Col xs={12} sm={12} md={12} lg={12}>
                    <span style={{fontSize: '15px', fontWeight: "bold"}}> Diagram Status RFP </span>
                </Col>
                <Col style={{alignSelf: 'center', padding: 0}}>
                  <Bar data={dataDiag} style={{width: 'auto'}} options={{ responsive: true ,maintainAspectRatio: true }} />
                </Col>
            </Row>
            </Col>
            <Col md={12}>

            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  )
}

export default Dashboard