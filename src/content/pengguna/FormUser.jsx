import React, { useEffect, useState } from 'react'
import { Button, Col, Form, Modal, Row } from 'react-bootstrap';

const FormUser = ({ show = false, close, state, alert, id, getApi }) => {
    console.log(id)

    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');
    const [roleId, setRoleId] = useState('');
    const [RoleList, setRoleList] = useState([]);
    const [userPassword, setUserPassword] = useState(false);
    const [validation, setValidation] = useState([
        { user: false, pass: false, nameUser: false, codeRole: false }
    ]);
    const getById = async () => {
        if (state === 'edit') {
            console.log("masuk datanya")
            const requestOptions = {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token'),
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': 'true'
                },
                
            };
            fetch('http://10.200.0.44:8080/api/auth/' + id, requestOptions)
                .then(response => response.json())
                .then(data => {
                    setUserName(data.userName)
                    setPassword(data.password)
                    setName(data.name)
                    setRoleId(data.roleId)
                })
        }
    }
    useEffect(() => {
        fetch('http://10.200.0.44:8080/api/RoleRfp', {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true'
            }
        })
            .then(response => response.json())
            .then(response => setRoleList(response))
    }, [])
    const FormData = () => {
        if (state === 'add' || state === 'edit') {
            return (
                <Row>
                    <Col md={12} style={{ marginBottom: '19px' }}>
                        <label> Username </label>
                        <input style={{ border: (validation[0].userName ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={userName} className='form-control'
                            onChange={(e) => setUserName(e.target.value)} />
                        {validation[0].userName ? (<span style={{ color: 'red' }}>Username Tidak Boleh Kosong</span>) : null}
                    </Col>
                    <Col md={12} style={{ marginBottom: '19px' }}>
                        <label for="exampleInputPassword1">Password</label>
                        <input style={{ border: (validation[0].password ? '1px solid red' : '1px solid #ced4da') }} onChange={(e) => setPassword(e.target.value)} type={userPassword ? 'text' : 'password'} className="form-control" placeholder="Password" />
                        <h6><input type="checkbox" onChange={(e) => setUserPassword(!userPassword)} /> Show Password</h6>
                        {validation[0].password ? (<span style={{ color: 'red' }}>password Tidak Boleh Kosong</span>) : null}
                    </Col>
                    <Col md={12} style={{ marginBottom: '19px' }}>
                        <label> name </label>
                        <input style={{ border: (validation[0].name ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={name} className='form-control'
                            onChange={(e) => setName(e.target.value)} />
                        {validation[0].name ? (<span style={{ color: 'red' }}>Name Tidak Boleh Kosong</span>) : null}
                    </Col>
                    <Col md={12} style={{ marginBottom: '19px' }}>
                        <label> Role </label>
                        <Form.Select style={{ border: (validation[0].codeRole ? '1px solid red' : '1px solid #ced4da') }} onChange={(e) => setRoleId(e.target.value)}>
                            <option value="" selected disabled>--Pilih Role--</option>
                            {RoleList.map((item) => (
                                <option value={item.id}>{item.name}</option>
                            )
                            )}
                        </Form.Select>
                        {validation[0].codeRole ? (<span style={{ color: 'red' }}>Role Tidak Boleh Kosong</span>) : null}
                    </Col>
                </Row>
            ) 
        } else {
            return 'Yakin Ingin Menghapus?'
        }
    }
    const saveData = async event => {
        event.preventDefault();
        if (state === 'add') {
            let user = userName;
            let pass = password;
            let nameUser = name;
            let codeRole = roleId;
            let validate = [...validation]
            let validateStatus = false;

            if (user === '') {
                validate[0].userName = true
                validateStatus = true
            }
            else {
                validate[0].userName = false
                validateStatus = false
            }
            if (pass === '') {
                validate[0].password = true
                validateStatus = true
            }
            else {
                validate[0].password = false
                validateStatus = false
            }
            if (nameUser === '') {
                validate[0].name = true
                validateStatus = true
            }
            else {
                validate[0].name = false
                validateStatus = false
            }
            if (codeRole === '') {
                validate[0].codeRole = true
                validateStatus = true
            }
            else {
                validate[0].codeRole = false
                validateStatus = false
            }
            setValidation(validate)
            if (validateStatus === false) {
                //api add
                const requestOptions = {
                    method: 'POST',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('token'),
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Credentials': 'true'
                    },
                    body: JSON.stringify({
                        userName: userName,
                        password: password,
                        name: name,
                        roleId: roleId
                    })
                };
                fetch('http://10.200.0.44:8080/api/auth/register', requestOptions)
                    .then(response => response.json())
                    .then(close)
                    .then(alert)
            }
        } else if (state === 'delete') {
            console.log('masuk')
            const requestOptions = {
                method: 'DELETE',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token'),
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': 'true'
                },
            };
            fetch(`http://10.200.0.44:8080/api/auth/${id}`, requestOptions)
                .then(response => response.json())
                .then(close)
                .then(alert)
        } else {
            //api update
            const requestOptions = {
                method: 'PUT',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token'),
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': 'true'
                },
                body: JSON.stringify({
                    userName: userName,
                    password: password,
                    name: name,
                    roleId: roleId
                })
            };
            fetch(`http://10.200.0.44:8080/api/auth/${id}`, requestOptions)
                .then(response => response.json())
                .then(close(!show))
                .then(alert)
        }
    }
    useEffect(() => {
        getById();
    }, [show]);

    return (
        <>
            <Modal show={show} onHide={close}>
                <Modal.Header closeButton>
                    <Modal.Title>{state} Project </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {FormData()}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={close}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={saveData}>
                        Save Data
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default FormUser
