import React, { useEffect, useState } from 'react'
import { Button, Col, Form, Modal, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { deleteCoa, editCoa, getCoaList, postCoa } from '../../redux/action/CoaAction';
import { getCoaCategoryList } from '../../redux/action/CoaCategoryAction';
import { toggleModal } from '../../redux/slice/CoaSlice';

const FormCoa = ({ show = false, close, state, alert, id, getApi }) => {
  const dispatch = useDispatch()
  const {dataById} = useSelector((state) => state.coa)
  const {dataCoaCategory} = useSelector((state) => state.coaCategory)
  const [accNumber, setAccNumber] = useState('');
  const [accName, setAccName] = useState('');
  const [descr, setDescr] = useState('');
  const [headerAcc, setHeaderAcc] = useState('');
  const [categoryId, setCategoryId] = useState('');
  const [categoryList, setCategoryList] = useState([]);
  const [validation, setValidation] = useState([
    { numberCoa: false, nameCoa: false, desc: false, headerCoa: false, codeCategory: false }
  ]);
  const param = {}

  useEffect(() => {
    dispatch(getCoaCategoryList())
  }, [])

  const FormData = () => {
    if (state === 'add') {
      return (
        <Row>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Acc Number </label>
            <input style={{ border: (validation[0].numberCoa ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={accNumber} className='form-control'
              onChange={(e) => setAccNumber(e.target.value)} />
            {validation[0].numberCoa ? (<span style={{ color: 'red' }}>Nomor Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Acc Name </label>
            <input style={{ border: (validation[0].nameCoa ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={accName} className='form-control'
              onChange={(e) => setAccName(e.target.value)} />
            {validation[0].nameCoa ? (<span style={{ color: 'red' }}>Nama Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Deskripsi </label>
            <input style={{ border: (validation[0].desc ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={descr} className='form-control'
              onChange={(e) => setDescr(e.target.value)} />
            {validation[0].desc ? (<span style={{ color: 'red' }}>Deskripsi Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Header Acc </label>
            <input style={{ border: (validation[0].headerCoa ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={headerAcc} className='form-control'
              onChange={(e) => setHeaderAcc(e.target.value)} />
            {validation[0].headerCoa ? (<span style={{ color: 'red' }}>Header Acc Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Nama Coa Category </label>
            <Form.Select style={{ border: (validation[0].codeCategory ? '1px solid red' : '1px solid #ced4da') }} onChange={(e) => setCategoryId(e.target.value)}>
              <option value="" selected disabled>--Pilih Coa Category--</option>
              {dataCoaCategory.map((item) => (
                <option value={item.id}>{item.name}</option>
              )
              )}
            </Form.Select>
            {validation[0].codeCategory ? (<span style={{ color: 'red' }}>Coa Category Tidak Boleh Kosong</span>) : null}
          </Col>
        </Row>
      )
    }
    if (state === 'edit') {
      return (
        <Row>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Acc Name </label>
            <input style={{ border: (validation[0].nameCoa ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={dataById.acc_name} className='form-control'
              onChange={(e) => setAccName(e.target.value)} />
            {validation[0].nameCoa ? (<span style={{ color: 'red' }}>Nama Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Deskripsi </label>
            <input style={{ border: (validation[0].desc ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={dataById.descr} className='form-control'
              onChange={(e) => setDescr(e.target.value)} />
            {validation[0].desc ? (<span style={{ color: 'red' }}>Deskripsi Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Header Acc </label>
            <input style={{ border: (validation[0].headerCoa ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={dataById.header_acc} className='form-control'
              onChange={(e) => setHeaderAcc(e.target.value)} />
            {validation[0].headerCoa ? (<span style={{ color: 'red' }}>Header Acc Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Nama Coa Category </label>
            <Form.Select style={{ border: (validation[0].codeCategory ? '1px solid red' : '1px solid #ced4da') }} onChange={(e) => setCategoryId(e.target.value)}>
              <option value="" selected disabled>--Pilih Coa Category--</option>
              {dataCoaCategory.map((item) => (
                <option value={item.id}>{item.name}</option>
              )
              )}
            </Form.Select>
            {validation[0].codeCategory ? (<span style={{ color: 'red' }}>Coa Category Tidak Boleh Kosong</span>) : null}
          </Col>
        </Row>
      )
    }
    else {
      return 'Yakin Ingin Menghapus?'
    }
  }
  const saveData = async event => {
    console.log(categoryId)
    event.preventDefault();
    let body = {
      acc_name : accName,
      descr : descr,
      header_acc : headerAcc,
      category_id : parseInt(categoryId)
    }
    param.data = body
    if (state === 'add') {
      let numberCoa = accNumber;
      let nameCoa = accName;
      let desc = descr;
      let headerCoa = headerAcc;
      let codeCategory = categoryId;
      let validate = [...validation]
      let validateStatus = false;
      if (numberCoa === '') {
        validate[0].numberCoa = true
        validateStatus = true
      }
      else {
        validate[0].numberCoa = false
        validateStatus = false
      }
      if (nameCoa === '') {
        validate[0].nameCoa = true
        validateStatus = true
      }
      else {
        validate[0].nameCoa = false
        validateStatus = false
      }
      if (desc === '') {
        validate[0].desc = true
        validateStatus = true
      }
      else {
        validate[0].desc = false
        validateStatus = false
      }
      if (headerCoa === '') {
        validate[0].headerCoa = true
        validateStatus = true
      }
      else {
        validate[0].headerCoa = false
        validateStatus = false
      }
      if (codeCategory === '') {
        validate[0].codeCategory = false
        validateStatus = false
      }
      setValidation(validate)
      if (validateStatus === false) {
        //api add
        param.data.id = accNumber
        dispatch(postCoa(param))
      }
    } else if (state === 'delete') {
      param.id = dataById.acc_number
      dispatch(deleteCoa(param))
    }
    else {
      //api update
      param.id = dataById.acc_number
      dispatch(editCoa(param))
    }
  }

  return (
    <>
      <Modal show={show} onHide={(e) => dispatch(toggleModal({ state: 'add' }))}>
        <Modal.Header closeButton>
          <Modal.Title>{state} Coa </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {FormData()}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={(e) => dispatch(toggleModal({ state: 'add' }))}>
            Close
          </Button>
          <Button variant="primary" onClick={saveData}>
            Save Data
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default FormCoa
