import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component';
import { Col, Container, Row, Spinner } from 'react-bootstrap'
import FormCoa from './FormCoa';
import { ButtonPrimary } from '../../component/Button';
import CardComponent from '../../component/CardComponent';
import BreadcrumbComponent from '../../component/BreadcrumbComponent';
import * as BsIcons from "react-icons/bs";
import { useDispatch, useSelector } from 'react-redux';
import { toggleModal } from '../../redux/slice/CoaSlice';
import { getCoaList } from '../../redux/action/CoaAction';

const Coa = () => {
    const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
    const dispatch = useDispatch()
    const {dataCoa,isLoading,isOpen,state} = useSelector((state) => state.coa)
    const columns = [
        {
            name: 'No',
            cell: (row, index) => index + 1,
            sortable: true,
        },
        {
            name: 'Acc Number',
            selector: row => row.acc_number,
            sortable: true,
            width: 'auto',
        },
        {
            name: 'Acc Name',
            selector: row => row.acc_name,
            sortable: true,
            width: 'auto',
        },
        {
            name: 'Deskripsi',
            selector: row => row.descr,
            sortable: true,
            width: 'auto',
        },
        {
            name: 'Header Acc',
            selector: row => row.header_acc,
            sortable: true,
            width: 'auto',
        },
        {
            name: 'Category Id',
            selector: row => row.detailCoaCategory.name,
            sortable: true,
            width: 'auto',
        },
        {
            name: 'Action',
            cell: row => (
                <>
                    <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'edit', data: row, id: row.acc_number }))}> <BsIcons.BsPencil /></ButtonPrimary>
                    <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'delete', data: row, id: row.acc_number }))}> <BsIcons.BsTrash /></ButtonPrimary>
                </>
            )
        }
    ];

    const [showModal, setShowModal] = useState(false)
    const alert = () => setMessage(!message)
    const [message, setMessage] = useState(false)
    const [stateForm, setStateForm] = useState('')
    const [idData, setIdData] = useState('')
    const openModal = (e, id, state) => {
        e.preventDefault();
        setShowModal(true);
        setStateForm(state);
        setIdData(id);
    }

    useEffect(() => {
        dispatch(getCoaList())
    }, []);

    return (

        <Container style={{ padding: '100px 0' }}>
            <h5 style={{ fontWeight: 'bold' }}>Coa</h5>
            <BreadcrumbComponent page={[{ path: '/coa', name: 'Coa' }]} />
            <CardComponent>
                {/* <Card.Body> */}
                <Row>
                    <Col md={6} style={{ textAlign: 'left' }}>
                        <h6>Daftar Coa</h6>
                    </Col>
                    <Col md={6} style={{ textAlign: 'right' }}>
                        <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'add' }))}>
                            <BsIcons.BsPlusCircle /> Add Data</ButtonPrimary>
                    </Col>
                    {message ? (
                        <Col md={12}>
                            <p>{stateForm} data berhasil</p>
                        </Col>) : null}
                    <Col md={12} className="text-center">
                        {isLoading ? <Spinner animation="border" variant="primary" size="" /> :
                            <DataTable
                                columns={columns}
                                data={dataCoa}
                                //  progressPending={pending}  
                                pagination
                                expandableRowsComponent={ExpandedComponent}
                            />}
                    </Col>
                </Row>
                {/* </Card.Body> */}
            </CardComponent>
            <FormCoa
                show={isOpen}
                close={setShowModal}
                alert={alert}
                state={state}
                id={idData}
            />
        </Container>
    );
}

export default Coa