import React, { useEffect, useState } from 'react'
import { Button, Col, Modal, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { deleteVendor, editVendor, getVendorList, postVendor } from '../../redux/action/VendorAction';
import { toggleModal } from '../../redux/slice/VendorSlice';

const FormVendor = ({ show = false, close, state, alert, id, getApi }) => {
  const dispatch = useDispatch()
  const { dataById } = useSelector((state) => state.vendor)
  const [name, setName] = useState('');
  const [descr, setDescr] = useState('');
  const [phone, setPhone] = useState('');
  const [validation, setValidation] = useState([
    { nameVendor: false, desc: false, phon: false }
  ])
  const param = {}
  const FormData = () => {
    if (state === 'add' || state === 'edit') {
      return (
        <Row>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Nama Vendor </label>
            <input type="text" defaultValue={dataById.name} className='form-control' onChange={(e) => setName(e.target.value)} />
            {validation[0].nameCategory ? (<span style={{ color: 'red' }}>Nama Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Deskripsi Vendor </label>
            <input type="text" defaultValue={dataById.descr} className='form-control' onChange={(e) => setDescr(e.target.value)} />
            {validation[0].nameCategory ? (<span style={{ color: 'red' }}>Nama Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Nomor Telefon </label>
            <input type="text" defaultValue={dataById.phone} className='form-control' onChange={(e) => setPhone(e.target.value)} />
            {validation[0].nameCategory ? (<span style={{ color: 'red' }}>Nama Tidak Boleh Kosong</span>) : null}
          </Col>
        </Row>
      )
    } else {
      return 'Yakin Hapus?'
    }
  }
  const saveData = async event => {
    event.preventDefault();
    let body = {
      name: name,
      descr: descr,
      phone: phone
    }
    param.data = body
    if (state === 'add') {
      let nameVendor = name;
      let desc = descr;
      let phon = phone;
      let validate = [...validation]
      let validateStatus = false;
      if (nameVendor === '') {
        validate[0].nameVendor = true
        validateStatus = true
      }
      else {
        validate[0].nameVendor = false
        validateStatus = false
      }
      if (desc === '') {
        validate[0].desc = true
        validateStatus = true
      }
      else {
        validate[0].desc = false
        validateStatus = false
      }
      if (phon === '') {
        validate[0].phon = true
        validateStatus = true
      }
      else {
        validate[0].phon = false
        validateStatus = false
      }
      setValidation(validate)
      if (validateStatus === false) {
        //api add 
        dispatch(postVendor(param))
      }
    } else if (state === 'delete') {
      param.id = dataById.id
      dispatch(deleteVendor(param))
    }
    else {
      //api update 
      param.id = dataById.id
      dispatch(editVendor(param))
    }
  }

  return (
    <>
      <Modal show={show} onHide={(e) => dispatch(toggleModal({ state: 'add' }))}>
        <Modal.Header closeButton>
          <Modal.Title>{state} Vendor </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {FormData()}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={(e) => dispatch(toggleModal({ state: 'add' }))}>
            Close
          </Button>
          <Button variant="primary" onClick={saveData}>
            Save Data
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default FormVendor