import React, { useEffect, useState } from 'react'
import { Button, Col, Modal, Row } from 'react-bootstrap';


const RoleId = ({ show = false, close, state, alert, id, getApi }) => {
    const [name, setName] = React.useState('');
    console.log(show)
    const [validation, setValidation] = React.useState([
        { nameRole: false }
    ]);
    console.log('ada')

    const FormData = () => {
        if (state === 'add' || state === 'edit') {
            return (
                <Row>
                    <Col md={12} style={{ marginBottom: '19px' }}>
                        <label> Role </label>
                        <input style={{ border: (validation[0].nameRole ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={name} className='form-control'
                            onChange={(e) => setName(e.target.value)} />
                        {validation[0].nameRole ? (<span style={{ color: 'red' }}>Role Tidak Boleh Kosong</span>) : null}
                    </Col>
                </Row>
            )
        }
    }
    const saveData = async event => {
        event.preventDefault();
        if (state === 'add') {
            let nameRole = name;
            let validate = [...validation]
            let validateStatus = false;
            if (nameRole === '') {
                validate[0].nameRole = true
                validateStatus = true
            }
            else {
                validate[0].nameRole = false
                validateStatus = false
            }
            setValidation(validate)
            if (validateStatus === false) {
                //api add 
                const requestOptions = {
                    method: 'POST',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('token'),
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Credentials': 'true'
                    },
                    body: JSON.stringify({
                        name: name
                    })
                };
                fetch('http://10.200.0.44:8080/api/RoleRfp', requestOptions)
                    .then(response => response.json())
                    .then(close(!show))
                    .then(alert)
            }
        }

        return (
            <Modal show={show} onHide={close}>
                <Modal.Header closeButton>
                    <Modal.Title>{state} Role </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {FormData()}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={close}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={saveData}>
                        Save Data
                    </Button>
                </Modal.Footer>
            </Modal>
        )
    }
}
export default RoleId