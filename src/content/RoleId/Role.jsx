import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component';
import { Col, Container, Row, Spinner } from 'react-bootstrap'
import { ButtonPrimary } from '../../component/Button';
import * as BsIcons from "react-icons/bs";
import BreadcrumbComponent from '../../component/BreadcrumbComponent';
import CardComponent from '../../component/CardComponent';
import FormRole from './FormRole';



const Role = () => {
    const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
    const [data, setData] = useState()
    const token = localStorage.getItem('token')
    const [loading, setLoading] = useState(false);
    const columns = [
        {
            name: 'No',
            cell: (row, index) => index + 1,
            width: "20%",
        },
        {
            name: 'Name',
            selector: row => row.name,
            sortable: true,
            width: "auto",
        },
        {
            name: 'Action',
            cell: row => (
                <>
                    <ButtonPrimary onClick={(e) => openModal(e, row.id, 'edit')}> <BsIcons.BsPencil /> </ButtonPrimary>
                    <ButtonPrimary onClick={(e) => openModal(e, row.id, 'delete')}> <BsIcons.BsTrash /> </ButtonPrimary>
                </>
            )
        }
    ];


    const getApiData = async () => {
        setLoading(true);
        const respone = await fetch("http://10.200.0.44:8080/api/RoleRfp", {
            method: 'get',
            headers: new Headers({

                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true',
            })
        })
            .then((respone) => respone.json());
        setData(respone)
        setLoading(false);
    };
    const [showModal, setShowModal] = useState(false)
    const alert = () => setMessage(!message)
    const [message, setMessage] = useState(false)
    const [stateForm, setStaeForm] = useState('')
    const [idData, setIdData] = useState('')
    const openModal = (e, id, state) => {
        e.preventDefault();
        setShowModal(true);
        setStaeForm(state);
        setIdData(id);
    }
    useEffect(() => {
        getApiData()
    }, [showModal]);

    return (

        <Container style={{ paddingTop: '100px', paddingBottom : 100}}>
            <h5 style={{ fontWeight: 'bold' }}> Role </h5>
            <BreadcrumbComponent page={[{ path: '/Role', name: 'Role' }]} />
            <CardComponent>
                {/* <Card.Body> */}
                <Row>
                    <Col md={6} style={{ textAlign: 'left' }}>
                        <h6>Daftar Role</h6>
                    </Col>
                    <Col md={6} style={{ textAlign: 'right' }}>
                        <ButtonPrimary onClick={(e) => openModal(e, '', 'add')}>
                            <BsIcons.BsPlusCircle /> Add Data</ButtonPrimary>
                    </Col>
                    {message ? (
                        <Col md={12}>
                            <p>{stateForm} data berhasil</p>
                        </Col>) : null}
                    <Col md={12} className="text-center">
                        {loading ? <Spinner animation="border" /> :
                            <DataTable
                                columns={columns}
                                data={data}
                                //  progressPending={pending}  
                                pagination
                                expandableRowsComponent={ExpandedComponent}
                            />
                        }
                    </Col>
                </Row>
                {/* </Card.Body> */}
            </CardComponent>
            <FormRole
                show={showModal}
                close={setShowModal}
                alert={alert}
                state={stateForm}
                id={idData}
            />
        </Container>
    );
}

export default Role

// https://blog.openreplay.com/creating-a-responsive-admin-dashboard-with-react