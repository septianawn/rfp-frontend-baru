import React, { useEffect, useState } from 'react'
import { Button, Col, Form, Modal, Row } from 'react-bootstrap';

const FormRfpDetail = ({show=false, close, state,alert, id,getApi}) => {
    // const [dataById, setDataById] = useState([]);
    // const [idDetail, setIdDetail] = useState('');
    const [rfpId, setRfpId] = useState('');
    const [accNumber, setAccNumber] = useState('');
    const [rekeningIdTujuan, setRekeningTujuanId] = useState(''); 
    // const [rfpDateDetail, setRfpDateDetail] = useState('');
    const [uraian, setUraian] = useState('');
    const [subTotal, setSubTotal] = useState('');
    const [paymentDate, setPaymentDate] = useState('');
    const [laporan, setLaporan] = useState('');
    const [bukti, setBukti] = useState('');
    const [rfpList, setRfpList] = useState([]);
    const [coaList, setCoaList] = useState([]);
    const [rekeningList, setRekeningList] = useState([]);
    const [validation, setValidation] = useState([
      {idDet: false, idRF: false, acn: false, rekTuj: false, dateDet: false,
    urai: false, subTo: false, payDate: false, lapor: false, buk: false}
    ]);
    const getById = async () => {
    if(state==='edit'){
      const requestOptions = {
        method: 'GET',
        headers: { 
          'Authorization': 'Bearer ' + localStorage.getItem('token'), 
            'Accept' : 'application/json',
            'Content-Type' : 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials' : 'true' 
          },
            // body: JSON.stringify({ name : name })
        };
        await fetch('http://10.200.0.44:8080/api/rfp-detail/'+id, requestOptions)
            .then(response => response.json())
            .then(data => {
                // setIdDetail(data.idDetail)
                setRfpId(data.rfpId)
                setAccNumber(data.accNumber)
                setRekeningTujuanId(data.rekeningIdTujuan)
                // setRfpDateDetail(data.rfpDateDetail)
                setUraian(data.uraian)
                setSubTotal(data.subTotal)
                setPaymentDate(data.paymentDate)
                setLaporan(data.laporan)
                setBukti(data.bukti)
            })
    }
  }
  useEffect(() => {
    fetch('http://10.200.0.44:8080/api/rfp/kategori?kategoriId=1', {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      }
    })
      .then(response => response.json())
      .then(response => setRfpList(response))
  }, [])
  useEffect(() => {
    fetch('http://10.200.0.44:8080/api/coa', {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      }
    })
      .then(response => response.json())
      .then(response => setCoaList(response))
  }, [])
  useEffect(() => {
    fetch('http://10.200.0.44:8080/api/rekening', {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      }
    })
      .then(response => response.json())
      .then(response => setRekeningList(response))
  }, [])

  const FormData = () =>{
    if(state==='add' || state==='edit'){
      return (
                <Row>
                <Col md={12} style={{ marginBottom: '19px' }}>
                <label> ID RFP </label>
                <Form.Select style={{border: (validation[0].rfpId ? '1px solid red' : '1px solid #ced4da')}} onChange={(e) => setRfpId(e.target.value)}>
                  <option value="" selected disabled>--Pilih Acc Number--</option>
                  {rfpList.map((item) => (
                      <option value={item.id}>{item.name}</option>
                    )
                  )}
                </Form.Select>
                {validation[0].rfpId ? (<span style={{color: 'red'}}>Rekening Tidak Boleh Kosong</span>) : null}
                </Col>
                <Col md={12} style={{ marginBottom: '19px' }}>
                <label> Acc Number </label>
                <Form.Select style={{border: (validation[0].accNumber ? '1px solid red' : '1px solid #ced4da')}} onChange={(e) => setAccNumber(e.target.value)}>
                  <option value="" selected disabled>--Pilih Acc Number--</option>
                  {coaList.map((item) => (
                      <option value={item.id}>{item.name}</option>
                    )
                  )}
                </Form.Select>
                {validation[0].accNumber ? (<span style={{color: 'red'}}>Rekening Tidak Boleh Kosong</span>) : null}
                </Col>
                <Col md={12} style={{ marginBottom: '19px' }}>
                <label> Rekening </label>
                <Form.Select style={{border: (validation[0].rekTuj ? '1px solid red' : '1px solid #ced4da')}} onChange={(e) => setRekeningTujuanId(e.target.value)}>
                  <option value="" selected disabled>--Pilih Rekening--</option>
                  {rekeningList.map((item) => (
                      <option value={item.id}>{item.name}</option>
                    )
                  )}
                </Form.Select>
                {validation[0].rekTuj ? (<span style={{color: 'red'}}>Rekening Tidak Boleh Kosong</span>) : null}
                </Col>
                <Col md={12} style={{marginBottom: '19px'}}>
                    <label> Uraian </label> 
                    <input style={{border: (validation[0].urai ? '1px solid red' : '1px solid #ced4da')}} type="text" defaultValue={uraian} className='form-control' 
                    onChange={(e)=>setUraian(e.target.value)} />
                    {validation[0].urai ? (<span style={{color: 'red'}}>Uraian Tidak Boleh Kosong</span>) : null}
                </Col>
                <Col md={12} style={{marginBottom: '19px'}}>
                    <label> Total  </label> 
                    <input style={{border: (validation[0].subTo ? '1px solid red' : '1px solid #ced4da')}} type="text" defaultValue={subTotal} className='form-control' 
                    onChange={(e)=>setSubTotal(e.target.value)} />
                    {validation[0].subTo ? (<span style={{color: 'red'}}>Total Tidak Boleh Kosong</span>) : null}
                </Col>
                <Col md={12} style={{marginBottom: '19px'}}>
                    <label> Tanggal Pembayaran  </label> 
                    <input style={{border: (validation[0].payDate ? '1px solid red' : '1px solid #ced4da')}} type="text" defaultValue={paymentDate} className='form-control' 
                    onChange={(e)=>setPaymentDate(e.target.value)} />
                    {validation[0].payDate ? (<span style={{color: 'red'}}>Total Tidak Boleh Kosong</span>) : null}
                </Col>
                <Col md={12} style={{marginBottom: '19px'}}>
                    <label> Laporan </label> 
                    <input style={{border: (validation[0].lapor ? '1px solid red' : '1px solid #ced4da')}} type="text" defaultValue={laporan} className='form-control' 
                    onChange={(e)=>setLaporan(e.target.value)} />
                    {validation[0].lapor ? (<span style={{color: 'red'}}>Laporan Tidak Boleh Kosong</span>) : null}
                </Col>
                <Col md={12} style={{marginBottom: '19px'}}>
                    <label> Bukti </label> 
                    <input style={{border: (validation[0].buk ? '1px solid red' : '1px solid #ced4da')}} type="text" defaultValue={bukti} className='form-control' 
                    onChange={(e)=>setBukti(e.target.value)} />
                    {validation[0].buk ? (<span style={{color: 'red'}}>Bukti Tidak Boleh Kosong</span>) : null}
                </Col>
            </Row>
      )
    }else{
      return 'Yakin Ingin Menghapus?'
    }
  }
    const saveData = async event =>{
      event.preventDefault();
        if(state==='add'){
          let idRF = rfpId;
          let acn = accNumber;
          let rekTuj = rekeningIdTujuan;
          let urai = uraian;
          let subTo = subTotal;
          let lapor = laporan;
          let payDate = paymentDate;
          let buk = bukti;
          let validate = [...validation]
          let validateStatus = false;
          if(idRF === ''){
            validate[0].idRF = true
            validateStatus = true
          }
          else{
            validate[0].idRF = false
            validateStatus = false
          }
          if(acn === ''){
            validate[0].acn = true
            validateStatus = true
          }
          else{
            validate[0].acn = false
            validateStatus = false
          }
          if(rekTuj === ''){
            validate[0].rekTuj = true
            validateStatus = true
          }
          else{
            validate[0].rekTuj = false
            validateStatus = false
          }
          if(urai === ''){
            validate[0].urai = true
            validateStatus = true
          }
          else{
            validate[0].urai = false
            validateStatus = false
          }
          if(subTo === ''){
            validate[0].subTo = true
            validateStatus = true
          }
          else{
            validate[0].subTo = false
            validateStatus = false
          }
          if(payDate === ''){
            validate[0].payDate = true
            validateStatus = true
          }
          else{
            validate[0].payDate = false
            validateStatus = false
          }
          if(lapor === ''){
            validate[0].lapor = true
            validateStatus = true
          }
          else{
            validate[0].lapor = false
            validateStatus = false
          }
          if(buk === ''){
            validate[0].buk = true
            validateStatus = true
          }
          else{
            validate[0].buk = false
            validateStatus = false
          }
          setValidation(validate)
          //api add
          if(validateStatus === false){
            //api add
            const requestOptions = {
              method: 'POST',
              headers: { 
                'Authorization': 'Bearer ' + localStorage.getItem('token'), 
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials' : 'true' 
              },
              body: JSON.stringify({ 
                rfpId : rfpId,
                accNumber : accNumber,
                rekeningIdTujuan : rekeningIdTujuan,
                uraian : uraian,
                subTotal : subTotal,
                paymentDate : paymentDate,
                laporan : laporan,
                bukti : bukti 
              })
          };
          fetch('http://10.200.0.44:8080/api/rfp-detail/', requestOptions)
              .then(response => response.json())
              .then(close(!show))
              .then(alert)
        }
          }else if(state==='delete'){
            const requestOptions = {
              method: 'DELETE',
              headers: { 
                'Authorization': 'Bearer ' + localStorage.getItem('token'), 
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials' : 'true' 
              },
          };
          fetch(`http://10.200.0.44:8080/api/rfp-detail/${id}`, requestOptions)
              .then(response => response.json())
              .then(close(!show))
              .then(alert)
          }else{
            //api update
            const requestOptions = {
              method: 'POST',
              headers: { 
                'Authorization': 'Bearer ' + localStorage.getItem('token'), 
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials' : 'true' 
              },
              body: JSON.stringify({ 
                rfpId : rfpId,
                accNumber : accNumber,
                rekeningIdTujuan : rekeningIdTujuan,
                uraian : uraian,
                subTotal : subTotal,
                paymentDate : paymentDate,
                laporan : laporan,
                bukti : bukti 
              })
          };
          fetch(`http://10.200.0.44:8080/api/rfp-detail/%{id}`, requestOptions)
              .then(response => response.json())
              .then(close(!show))
              .then(alert)
        }
    }

    useEffect(()=>{
      getById();
    },[show]);
  
    return (
      <>
        <Modal show={show} onHide={close}>
          <Modal.Header closeButton>
            <Modal.Title>{state} RFP DETAIL </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {FormData()}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={close}>
              Close
            </Button>
            <Button variant="primary" onClick={saveData}>
              Save Data
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    )
}

export default FormRfpDetail
