import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component';
import { Col, Container, Row, Spinner } from 'react-bootstrap'
import { ButtonPrimary } from '../../component/Button';
import * as BsIcons from "react-icons/bs";
import BreadcrumbComponent from '../../component/BreadcrumbComponent';
import { useParams } from 'react-router';
import FormRfpTransaksi from '../rfpTransaksi/FormRfpTransaksi';
import Card from 'react-bootstrap/Card'

const ContentComponent = ({ title, value }) => {
    return (
        <tr>
            <td style={{ width: '15%' }}>{title}</td>
            <td style={{ width: '3%' }}>:</td>
            <td style={{ width: '82%', fontWeight: 'bold' }}>{value}</td>
        </tr>
    )
}

const RfpDetail = () => {
    const { idRFP } = useParams()
    const [data, setData] = useState()
    const [dataTr, setDataTr] = useState()
    const [loading, setLoading] = useState(false);
    const [showModal, setShowModal] = useState(false)
    const [message, setMessage] = useState(false)
    const [stateForm, setStaeForm] = useState('')
    const [idData, setIdData] = useState('')
    const [dataRfp, setDataRfp] = useState(null)

    const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
    const alert = () => setMessage(!message)

    const columns = [
        {
            name: 'No',
            cell: (row, index) => index + 1,
            width: '5%',
        },
        {
            name: 'ACC Number',
            selector: row => row.accNumber,
            sortable: true,
        },
        {
            name: 'Uraian',
            selector: row => row.uraian,
            sortable: true,
        },
        {
            name: 'Total',
            selector: row => row.subTotal,
            sortable: true,
        },
        {
            name: 'Tanggal Pembayaran',
            selector: row => row.paymentDate,
            sortable: true,
        },
        {
            name: 'Rekening Tujuan',
            selector: row => row.rekeningIdTujuan,
            sortable: true,
        },
        {
            name: 'Laporan',
            selector: row => row.laporan,
            sortable: true,
        },
        {
            name: 'Bukti',
            selector: row => row.bukti,
            sortable: true,
        },
        {
            name: 'Action',
            cell: row => (
                <>
                    <ButtonPrimary onClick={(e) => openModal(e, row.id, 'edit')}> <BsIcons.BsPencil /></ButtonPrimary>
                </>
            ),
            width: 'auto'
        }
    ];

    const columnsTransaksi = [
        {
            name: 'No',
            cell: (row, index) => index + 1,
            sortable: true,
            width: '8%'
        },
        {
            name: 'Tanggal Transaksi',
            selector: row => row.transaksiDate,
            sortable: true,
            width: 'auto'
        },
        {
            name: 'Uraian',
            selector: row => row.uraian,
            sortable: true,
            width: 'auto'
        },
        {
            name: 'Penerimaan',
            selector: row => row.penerimaan,
            sortable: true,
            width: 'auto'
        },
        {
            name: 'Jumlah',
            selector: row => row.qty,
            sortable: true,
            width: 'auto'
        },
        {
            name: 'Harga',
            selector: row => row.harga,
            sortable: true,
            width: 'auto'
        },
        {
            name: 'Pengeluaran',
            selector: row => row.pengeluaran,
            sortable: true,
            width: 'auto'
        },
        {
            name: 'Bukti',
            selector: row => row.bukti,
            sortable: true,
            width: 'auto'
        }];

    useEffect(() => {
        setLoading(true);
        getApiDataRfp()
        getApiData()
        getApiDataTr()
        setLoading(false);
    }, [])

    const getApiDataRfp = async () => {
        const response = await fetch("http://10.200.0.44:8080/api/rfp/" + idRFP, {
            method: 'get',
            headers: new Headers({

                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true',
            })
        })

        if (response.status === 200) {
            const responseJson = await response.json()
            console.log(responseJson)
            setDataRfp(responseJson)
        }
        else {
            console.log('api error : ' + response.status)
        }
    };

    const getApiData = async () => {
        const respone = await fetch("http://10.200.0.44:8080/api/rfp-detail/all?rfpId=" + idRFP, {
            method: 'get',
            headers: new Headers({
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true',
            })
        })
            .then((respone) => respone.json());
        setData(respone)
    };

    const openModal = (e, id, state) => {
        e.preventDefault();
        setShowModal(true);
        setStaeForm(state);
        setIdData(id);
    }

    const getApiDataTr = async () => {
        const respone = await fetch("http://10.200.0.44:8080/api/rfp-transaksi?rfpId=" + idRFP, {
            method: 'get',
            headers: new Headers({
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true',
            })
        })
            .then((respone) => respone.json());
        setDataTr(respone)
    };

    return (

        <Container style={{ padding: '100px 0' }}>
            <h5 style={{ fontWeight: 'bold' }}>RFP DETAIL</h5>
            <BreadcrumbComponent page={[{ path: '/rfpDetail', name: 'RFP DETAIL' }]} />
                {/* <Card.Body> */}
                <Row>
                    <Card style={{ marginVertical: 10, class: 'border border-dark'}}>
                        <Card.Body>
                            <table className="table table-borderless">
                                <tbody>
                                    <ContentComponent title="NO RFP" value={dataRfp != null ? dataRfp.rfpNo : ""} />
                                    <ContentComponent title="Pemohon" value={dataRfp != null ? dataRfp.detailUser.name : ""} />
                                    <ContentComponent title="Tanggal" value={dataRfp != null ? dataRfp.rfpDate : ""} />
                                    <ContentComponent title="Uraian" value={dataRfp != null ? dataRfp.uraian : ""} />
                                    <ContentComponent title="Total" value={dataRfp != null ? dataRfp.subTotal : ""} />
                                    <ContentComponent title="Status RFP" value={dataRfp != null ? dataRfp.status : ""} />
                                </tbody>
                            </table>
                        </Card.Body>
                    </Card>

                    <Card style={{ marginTop: 15 }} class="border border-dark">
                        <Col md={12} className="text-center">
                            {loading ? <Spinner animation="border" /> :
                                <DataTable
                                    columns={columns}
                                    data={data}
                                    expandableRowsComponent={ExpandedComponent}
                                />
                            }
                        </Col>
                    </Card>
                    

                    <Card style={{padding: '10px 20px', marginTop: 15}}>
                        <Row>
                            
                            <Col md={6} style={{ alignSelf: 'center' }}>
                                <h6> Transaksi </h6>
                            </Col>
                            <Col md={6} style={{ textAlign: 'right',alignSelf: 'center' }}>
                                <ButtonPrimary onClick={(e) => openModal(e, '', 'add', getApiDataTr())}>
                                    <BsIcons.BsPlusCircle /> Add Data</ButtonPrimary>
                            </Col>
                            {message ? (
                            <Col md={12}>
                                <p>{stateForm} data berhasil</p>
                            </Col>) : null}
                            <Col md={12} className="text-center">
                                
                                {loading ? <Spinner animation="border" /> :
                                    <DataTable
                                        columns={columnsTransaksi}
                                        data={dataTr}
                                        //  progressPending={pending}  
                                        // pagination
                                        expandableRowsComponent={ExpandedComponent}
                                    />
                                }
                            </Col>
                        </Row>
                    </Card>
                </Row>
                {/* </Card.Body> */}
            <FormRfpTransaksi
                show={showModal}
                close={setShowModal}
                alert={alert}
                state={stateForm}
                id={idData}
            />
        </Container>
    );
}

export default RfpDetail