import React, { useEffect, useState } from 'react'
import { Button, Col, Modal, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { editDivision, getDivisionList, postDivision ,deleteDivision} from '../../redux/action/DivisionAction';
import { toggleModal } from '../../redux/slice/DivisionSlice';

const FormDivision = ({ show = false, close, state, alert, id, getApi }) => {
  const dispatch = useDispatch()
  const { dataById } = useSelector((state) => state.division)
  const [name, setName] = useState('');
  const [descr, setDescr] = useState('');
  const [validation, setValidation] = useState([
    { nameDiv: false, desc: false }
  ]);
  const param = {}

  const FormData = () => {
    if (state === 'add' || state === 'edit') {
      return (
        <Row>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Nama Divisi </label>
            <input style={{ border: (validation[0].nameDiv ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={dataById.name} className='form-control'
              onChange={(e) => setName(e.target.value)} />
            {validation[0].nameDiv ? (<span style={{ color: 'red' }}>Nama Divisi Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Deskripsi </label>
            <input style={{ border: (validation[0].noRek ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={dataById.descr} className='form-control'
              onChange={(e) => setDescr(e.target.value)} />
            {validation[0].desc ? (<span style={{ color: 'red' }}>Deskripsi Tidak Boleh Kosong</span>) : null}
          </Col>
        </Row>
      )
    } else {
      return 'Yakin Ingin Menghapus?'
    }
  }
  const saveData = async event => {
    event.preventDefault();
    let body = {
      name: name,
      descr: descr
    }
    param.data = body
    if (state === 'add') {
      let nameDiv = name;
      let desc = descr;
      let validate = [...validation]
      let validateStatus = false;
      if (nameDiv === '') {
        validate[0].nameDiv = true
        validateStatus = true
      }
      else {
        validate[0].nameDiv = false
        validateStatus = false
      }
      if (desc === '') {
        validate[0].desc = true
        validateStatus = true
      }
      else {
        validate[0].desc = false
        validateStatus = false
      }
      setValidation(validate)
      //api add
      if (validateStatus === false) {
        //api add
        dispatch(postDivision(param))
      }
    } else if (state === 'delete') {
      param.id = dataById.id
      dispatch(deleteDivision(param))
    } else {
      param.id = dataById.id
      dispatch(editDivision(param))
    }
  }

  return (
    <>
      <Modal show={show} onHide={(e) => dispatch(toggleModal({ state: 'add' }))}>
        <Modal.Header closeButton>
          <Modal.Title>{state} Division </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {FormData()}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={(e) => dispatch(toggleModal({ state: 'add' }))}>
            Close
          </Button>
          <Button variant="primary" onClick={saveData}>
            Save Data
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default FormDivision
