import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component';
import { Col, Container, Row, Spinner } from 'react-bootstrap'
import FormDivision from './FormDivision';
import CardComponent from '../../component/CardComponent';
import BreadcrumbComponent from '../../component/BreadcrumbComponent';
import { ButtonPrimary } from '../../component/Button';
import * as BsIcons from "react-icons/bs";
import { useDispatch, useSelector } from 'react-redux';
import { toggleModal } from '../../redux/slice/DivisionSlice';
import { getDivisionList } from '../../redux/action/DivisionAction';

const Division = () => {
    const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
    const dispatch = useDispatch()
    const {dataDivision,isLoading,isOpen,state} = useSelector((state) => state.division)
    const [loading, setLoading] = useState(false);
    const columns = [
        {
            name: 'No',
            cell: (row, index) => index + 1,
            width:'8%',
        },
        {
            name: 'Name',
            selector: row => row.name,
            sortable: true,
            width:'auto',
        },
        {
            name: 'Descr',
            selector: row => row.descr,
            sortable: true,
            width:'auto',
        },
        {
            name: 'Action',
            cell: row => (
                <>
                <ButtonPrimary onClick={(e)=> dispatch(toggleModal({state: 'edit',data : row, id: row.id}))}> <BsIcons.BsPencil/></ButtonPrimary> 
                <ButtonPrimary onClick={(e)=> dispatch(toggleModal({state: 'delete',data : row, id: row.id}))}> <BsIcons.BsTrash/></ButtonPrimary>
                </>
            )
        }
    ];

    
    const [showModal, setShowModal] = useState(false)
    const alert = () => setMessage(!message)
    const [message, setMessage] = useState(false)
    const [stateForm, setStateForm] = useState('')
    const [idData, setIdData] = useState('')
    const openModal = (e, id, state) => {
        e.preventDefault();
        setShowModal(true);
        setStateForm(state);
        setIdData(id);
    }


    useEffect(() => {
        dispatch(getDivisionList())
    }, []);

    return (

        <Container style={{ padding: '100px 0' }}>
            <h5 style={{fontWeight: 'bold'}}>Division</h5>
            <BreadcrumbComponent page={[{ path: '/division', name: 'Division' }]} />
            <CardComponent>
                {/* <Card.Body> */}
                <Row>
                    <Col md={6} style={{ textAlign: 'left' }}>
                        <h6>Daftar Divisi</h6>
                    </Col>
                    <Col md={6} style={{ textAlign: 'right' }}>
                        <ButtonPrimary onClick={(e) => dispatch(toggleModal({state: 'add'}))}> 
                        <BsIcons.BsPlusCircle/> Add Data</ButtonPrimary>
                    </Col>
                    {message ? (
                    <Col md={12}>
                        <p>{stateForm} data berhasil</p>
                    </Col>) : null}
                    <Col md={12} className="text-center">

                         {isLoading ? <Spinner animation="border" /> :
                            <DataTable
                                columns={columns}
                                data={dataDivision}
                                //  progressPending={pending}  
                                pagination
                                expandableRowsComponent={ExpandedComponent}
                            />
                        }

                    </Col>
                </Row>
                {/* </Card.Body> */}
            </CardComponent>
            <FormDivision
                show={isOpen}
                close={setShowModal}
                alert={alert}
                state={state}
                id={idData}
            />
        </Container>
    );
}

export default Division