import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component';
import { Button, Col, Container, Row, Spinner } from 'react-bootstrap'
import CardComponent from '../../component/CardComponent';
import BreadcrumbComponent from '../../component/BreadcrumbComponent';
import { ButtonPrimary } from '../../component/Button';
import * as BsIcons from "react-icons/bs";
import FormRfpUmum from './FormRfpUmum';
import { Link } from 'react-router-dom';

const RfpUmum = () => {
    const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false);
    const columns = [
        {
            name: 'No',
            cell: (row, index) => index + 1,
            width: '5%',
        },
        {
            name: 'Rfp No',
            selector: row => row.rfpNo,
            sortable: true,
        },
        {
            name: 'Pemohon',
            selector: row => row.detailUser.name,
            sortable: true,
        },
        {
            name: 'Rfp Date',
            cell: row => (
                row.rfpDate.split('T')[0]
            )
        },
        {
            name: 'Uraian',
            selector: row => row.uraian,
            sortable: true,
        },
        {
            name: 'Sub Total',
            selector: row => row.subTotal,
            sortable: true,
        },
        {
            name: 'Project',
            selector: row => row.detailProject.name,
            sortable: true,
        },
        {
            name: 'Status',
            selector: row => row.status,
            sortable: true,
        },
        {
            name: 'Action',
            cell: row => (
                <>
                    {/* <ButtonPrimary onClick={(e)=> openModal(e, row.id, 'edit')}> <BsIcons.BsPencil /> </ButtonPrimary>
                    <ButtonPrimary onClick={(e)=> openModal(e, row.id, 'delete')}> <BsIcons.BsTrash /> </ButtonPrimary> */}
                    {/* <ButtonPrimary onClick={()=> window.location.href= '/rfpDetail'}> <BsIcons.BsEye /> </ButtonPrimary> */}
                    <Link to={'/rfpDetail/' + row.id}>
                        <ButtonPrimary
                        // onClick={(e) => window.location.href = '/rfpDetail/'}
                        >
                            <BsIcons.BsEye />
                        </ButtonPrimary>
                    </Link>
                </>
            )
        }
    ];


    const getApiData = async () => {
        setLoading(true);
        const respone = await fetch("http://10.200.0.44:8080/api/rfp/kategori?kategoriId=2", {
            method: 'get',
            headers: new Headers({

                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': 'true',
            })
        })
            .then((respone) => respone.json());
        setData(respone)
        setLoading(false);
    };
    const [showModal, setShowModal] = useState(false)
    const alert = () => setMessage(!message)
    const [message, setMessage] = useState(false)
    const [stateForm, setStaeForm] = useState('')
    const [idData, setIdData] = useState('')
    const openModal = (e, id, state) => {
        e.preventDefault();
        setShowModal(true);
        setStaeForm(state);
        setIdData(id);
    }

    useEffect(() => {
        getApiData()
    }, [showModal]);

    return (

        <Container style={{ paddingTop: '100px', paddingBottom : 100 }}>
            <h5 style={{ fontWeight: 'bold' }}>Rfp Umum</h5>
            <BreadcrumbComponent page={[{ path: '/rfpUmum', name: 'Rfp Umum' }]} />
            <CardComponent>
                {/* <Card.Body> */}
                <Row>
                    <Col md={6} style={{ textAlign: 'left' }}>
                        <h6>Daftar Rfp Umum</h6>
                    </Col>
                    <Col md={6} style={{ textAlign: 'right' }}>
                        <ButtonPrimary onClick={(e) => openModal(e, '', 'add', getApiData())}>
                            <BsIcons.BsPlusCircle /> Add Data</ButtonPrimary>
                    </Col>
                    {message ? (
                        <Col md={12}>
                            <p>{stateForm} data berhasil</p>
                        </Col>) : null}
                    <Col md={12} className="text-center">
                        {loading ? <Spinner animation="border" /> :
                            <DataTable
                                columns={columns}
                                data={data}
                                //  progressPending={pending}  
                                pagination
                                expandableRowsComponent={ExpandedComponent}
                            />
                        }
                    </Col>
                </Row>
                {/* </Card.Body> */}
            </CardComponent>
            <FormRfpUmum
                show={showModal}
                close={setShowModal}
                alert={alert}
                state={stateForm}
                id={idData}
            />
        </Container>
    );
}

export default RfpUmum