import React, { useEffect, useState } from 'react'
import { Button, Col, Form, Modal, Row } from 'react-bootstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const FormRfpUmum = ({ show = false, close, state, alert, id, getApi }) => {
  const [pemohonId, setPemohonId] = useState('');
  const [rfpDate, setTanggalRfp] = useState(new Date());
  const [divisionId, setDivisi] = useState('');
  const [rekeningId, setRekening] = useState('');
  const [laporan, setLaporan] = useState('');
  const [accNumber, setAccNumber] = useState('');
  const [uraian, setUraian] = useState('');
  const [subTotal, setSubTotal] = useState('');
  const [projectId, setProjectId] = useState('');
  const [rekeningIdTujuan, setRekeningTujuan] = useState('');
  const [pemohonList, setPemohonList] = useState([]);
  const [divisionList, setDivisionList] = useState([]);
  const [rekeningList, setRekeningList] = useState([]);
  const [accNumberList, setAccNumberList] = useState([]);
  const [projectList, setProjectList] = useState([])
  const [rekeningTujuanList, setRekeningTujuanList] = useState([]);
  const [validation, setValidation] = useState([
    {
      namaPemohon: false, tanggal: false, divisi: false, rekening: false, laporanData: false, accNo: false, uraianData: false, total: false, project: false, rekeningTujuan: false
    }
  ]);
  console.log(state);
  const getById = async () => {
    if (state === 'edit') {
      const requestOptions = {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + localStorage.getItem('token'),
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': 'true'
        },
        // body: JSON.stringify({ name : name })
      };
      await fetch('http://10.200.0.44:8080/api/rfp/' + id, requestOptions)
        .then(response => response.json())
        .then(data => {
          setPemohonId(data.pemohonId)
          setTanggalRfp(data.rfpDate)
          setDivisi(data.divisionId)
          setRekening(data.rekeningId)
          setLaporan(data.laporan)
          setAccNumber(data.accNumber)
          setUraian(data.uraian)
          setSubTotal(data.subTotal)
          setProjectId(data.projectId)
          setRekeningTujuan(data.rekeningIdTujuan)
        })
    }
  }
  useEffect(() => {
    fetch('http://10.200.0.44:8080/api/division', {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      }
    })
      .then(response => response.json())
      .then(response => setDivisionList(response))
  }, [])
  useEffect(() => {
    fetch('http://10.200.0.44:8080/api/auth/', {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      }
    })
      .then(response => response.json())
      .then(response => setPemohonList(response))
  }, [])
  useEffect(() => {
    fetch('http://10.200.0.44:8080/api/rekening?isCompany=true', {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      }
    })
      .then(response => response.json())
      .then(response => setRekeningList(response))
  }, [])
  useEffect(() => {
    fetch('http://10.200.0.44:8080/api/coa', {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      }
    })
      .then(response => response.json())
      .then(response => setAccNumberList(response))
  }, [])
  useEffect(() => {
    fetch('http://10.200.0.44:8080/api/project', {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      }
    })
      .then(response => response.json())
      .then(response => setProjectList(response))
  }, [])
  useEffect(() => {
    fetch('http://10.200.0.44:8080/api/rekening?isCompany=false', {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      }
    })
      .then(response => response.json())
      .then(response => setRekeningTujuanList(response))
  }, [])
  const FormData = () => {
    if (state === 'add' || state === 'edit') {
      return (
        <Row>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Pemohon </label>
            <Form.Select style={{ border: (validation[0].namaPemohon ? '1px solid red' : '1px solid #ced4da') }}
            onChange={(e) => setPemohonId(e.target.value)}>
              <option value="" selected disabled>-- Pilih Pemohon --</option>
              {pemohonList.map((item) => (
                <option value={item.id}>{item.name}</option>
                )
              )}
            </Form.Select>
            {validation[0].namaPemohon ? (<span style={{ color: 'red' }}>Pemohon Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Tanggal Rfp </label>
            <DatePicker
              selected={rfpDate} dateFormat="yyyy-MM-dd" timeFormat='HH:mm'
              onChange={(date) => { const d = new Date(date); setTanggalRfp(d); }} style={{ border: (validation[0].tanggal ? '1px solid red' : '1px solid #ced4da')} }className='form-control' />
            {validation[0].tanggal ? (<span style={{ color: 'red' }}>Tanggal Rfp Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Divisi </label>
            <Form.Select style={{ border: (validation[0].divisi ? '1px solid red' : '1px solid #ced4da') }}
            onChange={(e) => setDivisi(e.target.value)}>
              <option value="" selected disabled>-- Pilih Divisi --</option>
              {divisionList.map((item) => (
                <option value={item.id}>{item.descr}</option>
                )
              )}
            </Form.Select>
            {validation[0].divisi ? (<span style={{ color: 'red' }}>Divisi Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Rekening </label>
            <Form.Select 
            style={{ border: (validation[0].rekening ? '1px solid red' : '1px solid #ced4da') }} 
            onChange={(e) => setRekening(e.target.value)}>
              <option value="" selected disabled>-- Pilih Rekening --</option>
              {rekeningList.map((item) => (
                <option value={item.id}>{item.name}</option>
              )
              )}
            </Form.Select>
            {validation[0].rekening ? (<span style={{ color: 'red' }}>Rekening Tidak Boleh Kosong</span>) : null}
          </Col>
          {/* <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Laporan </label>
            <input type="text" defaultValue={laporan} className='form-control'
              onChange={(e) => setLaporan(e.target.value)} />
          </Col> */}
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Acc Number </label>
            <Form.Select style={{ border: (validation[0].accNo ? '1px solid red' : '1px solid #ced4da') }}
            onChange={(e) => setAccNumber(e.target.value)}>
              <option value="" selected disabled>-- Pilih Acc Number --</option>
              {accNumberList.map((item) => (
                <option value={item.id}>{item.acc_number}</option>
              )
              )}
            </Form.Select>
            {validation[0].accNo ? (<span style={{ color: 'red' }}>Rekening Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Uraian </label>
            <input type="text" defaultValue={uraian} className='form-control'
              onChange={(e) => setUraian(e.target.value)} />
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Sub Total </label>
            <input type="text" defaultValue={subTotal} className='form-control'
              onChange={(e) => setSubTotal(e.target.value)} />
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Project </label>
            <Form.Select style={{ border: (validation[0].project ? '1px solid red' : '1px solid #ced4da') }}
            onChange={(e) => setProjectId(e.target.value)}>
              <option value="" selected disabled>-- Pilih Project --</option>
              {projectList.map((item) => (
                <option value={item.id}>{item.name}</option>
              )
              )}
            </Form.Select>
            {validation[0].project ? (<span style={{ color: 'red' }}>Project Tidak Boleh Kosong</span>) : null}
          </Col>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Rekening Tujuan </label>
            <Form.Select style={{ border: (validation[0].rekeningTujuan ? '1px solid red' : '1px solid #ced4da') }}
            onChange={(e) => setRekeningTujuan(e.target.value)}>
              <option value="" selected disabled>-- Pilih Rekening Tujuan --</option>
              {rekeningTujuanList.map((item) => (
                <option value={item.id}>{item.name}</option>
              )
              )}
            </Form.Select>
            {validation[0].rekeningTujuan ? (<span style={{ color: 'red' }}>Rekening Tidak Boleh Kosong</span>) : null}
          </Col>
        </Row>
      )
    } else {
      return 'Yakin Ingin Menghapus?'
    }
  }
  const saveData = async event => {
    event.preventDefault();
    if (state === 'add') {
      let namaPemohon = pemohonId;
      let tanggal = rfpDate;
      let divisi = divisionId;
      let rekening = rekeningId;
      let laporanData = laporan;
      let accNo = accNumber;
      let uraianData = uraian;
      let total = subTotal;
      let project = projectId;
      let rekeningTujuan = rekeningIdTujuan;
      let validate = [...validation]
      let validateStatus = false;
      if (namaPemohon === '') {
        validate[0].namaPemohon = true
        validateStatus = true
      }
      else {
        validate[0].namaPemohon = false
        validateStatus = false
      }
      if (tanggal === '') {
        validate[0].tanggal = true
        validateStatus = true
      }
      else {
        validate[0].tanggal = false
        validateStatus = false
      }
      if (divisi === '') {
        validate[0].divisi = true
        validateStatus = true
      }
      else {
        validate[0].divisi = false
        validateStatus = false
      }
      if (rekening === '') {
        validate[0].rekening = true
        validateStatus = true
      }
      else {
        validate[0].rekening = false
        validateStatus = false
      }
      if (laporanData === '') {
        validate[0].laporanData = true
        validateStatus = true
      }
      else {
        validate[0].laporanData = false
        validateStatus = false
      }
      if (accNo === '') {
        validate[0].accNo = true
        validateStatus = true
      }
      else {
        validate[0].accNo = false
        validateStatus = false
      }
      if (uraianData === '') {
        validate[0].uraianData = true
        validateStatus = true
      }
      else {
        validate[0].uraianData = false
        validateStatus = false
      }
      if (total === '') {
        validate[0].total = true
        validateStatus = true
      }
      else {
        validate[0].total = false
        validateStatus = false
      }
      if (project === '') {
        validate[0].project = true
        validateStatus = true
      }
      else {
        validate[0].project = false
        validateStatus = false
      }
      if (rekeningTujuan === '') {
        validate[0].rekeningTujuan = true
        validateStatus = true
      }
      else {
        validate[0].rekeningTujuan = false
        validateStatus = false
      }
      setValidation(validate)
      
      if (validateStatus === false) {
        //api add
        let newDate = new Date(Date.parse(rfpDate));
        let month =  '' + (newDate.getMonth() + 1);
        if(month.length < 2){
          month = '0' + month;
        }
        let date = '' + newDate.getDate();
        if(date.length < 2){
          date = '0' + date;
        }

        newDate = newDate.getFullYear()+'-'+month+'-'+date;
        console.log(newDate)
        const requestOptions = {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': 'true'
          },
          body: JSON.stringify({
            pemohonId: pemohonId,
            rfpDate: newDate,
            divisionId: divisionId,
            rekeningId: rekeningId,
            laporan: laporan,
            accNumber: accNumber,
            uraian: uraian,
            subTotal: subTotal,
            projectId : projectId,
            rekeningIdTujuan: rekeningIdTujuan
          })
        };
        fetch('http://10.200.0.44:8080/api/rfp/umum', requestOptions)
          .then(response => response.json())
          .then(close)
          .then(alert)
      }
    } else if (state === 'delete') {
      const requestOptions = {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + localStorage.getItem('token'),
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': 'true'
        },
      };
      fetch(`http://10.200.0.44:8080/api/rfp/${id}`, requestOptions)
        .then(response => response.json())
        .then(close)
        .then(alert)
    } else {
      //api update
      const requestOptions = {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + localStorage.getItem('token'),
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': 'true'
        },
        body: JSON.stringify({
          pemohonId: pemohonId,
          rfpDate: rfpDate,
          divisionId: divisionId,
          rekeningId: rekeningId,
          laporan: laporan,
          accNumber: accNumber,
          uraian: uraian,
          subTotal: subTotal,
          projectId: projectId,
          rekeningIdTujuan: rekeningIdTujuan
        })
      };
      fetch(`http://10.200.0.44:8080/api/rfp/${id}`, requestOptions)
        .then(response => response.json())
        .then(close)
        .then(alert)
    }
  }
  useEffect(() => {
    getById();
  }, [show]);

  return (
    <>
      <Modal show={show} onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title>{state}  Rfp Umum </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {FormData()}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={close}>
            Close
          </Button>
          <Button variant="primary" onClick={saveData}>
            Save Data
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}


export default FormRfpUmum
