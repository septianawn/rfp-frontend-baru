import React, { useEffect, useState } from 'react'
import { Button, Col, Modal, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { deleteCoaCategory, editCoaCategory, postCoaCategory } from '../../redux/action/CoaCategoryAction';
import { toggleModal } from '../../redux/slice/CoaCategorySlice';

const FormCoaCategory = ({ show = false, close, state, alert, id, getApi }) => {
  const dispatch = useDispatch()
  const { dataByIdCoaCategory } = useSelector((state) => state.coaCategory)
  const [name, setName] = useState('');
  const [validation, setValidation] = useState([
    { nameCategory: false }
  ]);
  // console.log(name); 
  // console.log(state); 
  const param = {}

  const FormData = () => {
    if (state === 'add' || state === 'edit') {
      return (
        <Row>
          <Col md={12} style={{ marginBottom: '19px' }}>
            <label> Nama Coa Category </label>
            <input style={{ border: (validation[0].nameCategory ? '1px solid red' : '1px solid #ced4da') }} type="text" defaultValue={dataByIdCoaCategory.name} className='form-control'
              onChange={(e) => setName(e.target.value)} />
            {validation[0].nameCategory ? (<span style={{ color: 'red' }}>Nama Tidak Boleh Kosong</span>) : null}
          </Col>
        </Row>
      )
    }
    else {
      return 'Yakin Hapus?'
    }
  }
  const saveData = async event => {
    event.preventDefault();
    let body = {
      name: name
    }
    param.data = body
    if (state === 'add') {
      let nameCategory = name;
      let validate = [...validation]
      let validateStatus = false;
      if (nameCategory === '') {
        validate[0].nameCategory = true
        validateStatus = true
      }
      else {
        validate[0].nameCategory = false
        validateStatus = false
      }
      setValidation(validate)
      if (validateStatus === false) {
        //api add 
        dispatch(postCoaCategory(param))
      }
    } else if (state === 'delete') {
      param.id = dataByIdCoaCategory.id
      dispatch(deleteCoaCategory(param))
    }
    else {
      //api update 
      param.id = dataByIdCoaCategory.id
      dispatch(editCoaCategory(param))
    }
  }

  return (
    <>
      <Modal show={show} onHide={(e) => dispatch(toggleModal({ state: 'add' }))}>
        <Modal.Header closeButton>
          <Modal.Title>{state} Coa Category </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {FormData()}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={(e) => dispatch(toggleModal({ state: 'add' }))}>
            Close
          </Button>
          <Button variant="primary" onClick={saveData}>
            Save Data
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default FormCoaCategory