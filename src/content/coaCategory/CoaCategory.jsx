import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component';
import { Col, Container, Row, Spinner } from 'react-bootstrap'
import FormCoaCategory from './FormCoaCategory';
import CardComponent from '../../component/CardComponent';
import BreadcrumbComponent from '../../component/BreadcrumbComponent';
import { ButtonPrimary } from '../../component/Button';
import * as BsIcons from "react-icons/bs";
import { useDispatch, useSelector } from 'react-redux';
import { getCoaCategoryList } from '../../redux/action/CoaCategoryAction';
import { toggleModal } from '../../redux/slice/CoaCategorySlice';

const CoaCategory = () => {
    const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
    const dispatch = useDispatch()
    const { dataCoaCategory, isOpen, state, isLoading } = useSelector((state) => state.coaCategory)
    const columns = [
        {
            name: 'No',
            cell: (row, index) => index + 1,
            sortable: true,
            width: '8%'
        },
        {
            name: 'Name',
            selector: row => row.name,
            sortable: true,
            width: 'auto',
        },
        {
            name: 'Action',
            cell: row => (
                <>
                    <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'edit', data: row, id: row.id }))}> <BsIcons.BsPencil /></ButtonPrimary>
                    <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'delete', data: row, id: row.id }))}> <BsIcons.BsTrash /></ButtonPrimary>
                </>
            )
        }
    ];


    const [showModal, setShowModal] = useState(false)
    const alert = () => setMessage(!message)
    const [message, setMessage] = useState(false)
    const [stateForm, setStateForm] = useState('')
    const [idData, setIdData] = useState('')
    const openModal = (e, id, state) => {
        e.preventDefault();
        setShowModal(true);
        setStateForm(state);
        setIdData(id);
    }

    useEffect(() => {
        dispatch(getCoaCategoryList())
    }, []);

    return (

        <Container style={{ padding: '100px 0' }}>
            <h5 style={{ fontWeight: 'bold' }}>Coa Category</h5>
            <BreadcrumbComponent page={[{ path: '/coaCategory', name: 'CoaCategory' }]} />
            <CardComponent>
                {/* <Card.Body> */}
                <Row>
                    <Col md={6} style={{ textAlign: 'left' }}>
                        <h6>Daftar Coa Category</h6>
                    </Col>
                    <Col md={6} style={{ textAlign: 'right' }}>
                        <ButtonPrimary onClick={(e) => dispatch(toggleModal({ state: 'add' }))}>
                            <BsIcons.BsPlusCircle /> Add Data</ButtonPrimary>
                    </Col>
                    {message ? (
                        <Col md={12}>
                            <p>{stateForm} data berhasil</p>
                        </Col>) : null}
                    <Col md={12} className="text-center">
                        {isLoading ? <Spinner animation="border" /> :
                            <DataTable
                                columns={columns}
                                data={dataCoaCategory}
                                //  progressPending={pending}  
                                pagination
                                expandableRowsComponent={ExpandedComponent}
                            />
                        }
                    </Col>
                </Row>
                {/* </Card.Body> */}
            </CardComponent>
            <FormCoaCategory
                show={isOpen}
                close={setShowModal}
                alert={alert}
                state={state}
                id={idData}
            />
        </Container>
    );
}

export default CoaCategory